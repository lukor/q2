# Projektplan

Dieses Projekt besteht aus zwei Teilen mit jeweils mehreren Phasen.
Die beiden Teile werden parallel abgearbeitet, der geplante Aufwand beträgt hierbei durchschnittlich 5 Stunden / Teil / Person / Woche.

Es werden zusätzlich zu jeder Phase die geplanten Start- und End-Daten angegeben, diese gelten der Orientierung und bauen auf der jeweiligen Aufwandsschätzung auf.

## Abstrakte Maschine

Als erster Baustein der Ausführung von `q` Code existiert die abstrakte Maschine, welche die definierten Operationen und Schritte in einem in Haskell implementierten Interpreter ausführt.

Die Implementierung dessen wird in den folgenden Phasen durchgeführt:

| Phase       | Aufwand | Start        | Ende         |
|-------------|--------:|--------------|--------------|
| Setup       |     30h | 2021/04/12   | 2021/05/02   |
| Operationen |     35h | 2021/05/03   | 2021/05/27   |
| Blöcke      |     30h | 2021/05/28   | 2021/06/20   |
| Funktionen  |     25h | 2021/06/21   | 2021/07/11   |
| Stdlib      |     30h | 2021/07/12   | 2021/08/01   |
| *Gesamt*    |  *150h* | *2021/04/12* | *2021/06/27* |

### Setup

Das Setup beinhaltet den Aufbau des grundlegenden Interpreter Loops, inklusive der Datenstrukturen des Working-Stacks und anderen notwendigen Hilfsfunktionen.

### Operationen

Hierbei werden die simplen Operationen (wie in der Spezifikation definiert) implementiert.

### Blöcke

Um Control Flow und Funktionsdefinitionen ermöglichen zu können muss das Einlesen und Verarbeiten von Blöcken möglich sein, welches in dieser Phase implementiert wird.
Weiters wird auch die Control Flow Anweisung `Execute` in dieser Phase implementiert.

### Funktionen

Abschließend werden Funktionsaufrufe umgesetzt, welche sowohl die Benennung und permanente Speicherung definierter Blöcke beinhaltet, als auch den darauf folgenden Aufruf ermöglicht.

### Stdlib

Dieser Block inkludiert die Standard Library der Sprache. Einzelne Funktionen
werden schon in den anderen Blöcken implementiert, hier werden dann weitere
Funktionen für die volle Funktionsfähigkeit der Sprache nachgereicht (Networking,...).

## Dynamischer Übersetzer

Um eine schnellere Abarbeitung zu ermöglichen, kann die Just-In-Time Kompilation verwendet werden.
Dies wird bei dem Start des Programmes als Option übergeben und ersetzt die Interpretation durch eine Kompilation und Ausführung des entstandenen Maschinencodes.

Die folgenden Phasen der Entwicklung sind dabei geplant:

| Phase            | Aufwand | Start        | Ende         |
|------------------|--------:|--------------|--------------|
| Setup            | 40h     |  2021/04/12  |  2021/05/09  |
| Operationen      | 15h     |  2021/05/10  |  2021/05/23  |
| Control Flow     | 15h     |  2021/05/24  |  2021/06/06  |
| Funktionsaufrufe | 30h     |  2021/06/07  |  2021/06/27  |
| *Gesamt*         | *100h*  | *2021/04/12* | *2021/06/27* |

### Setup

Ähnlich zu dem Setup der abstrakten Maschine werden hier die grundlegenden Datenstrukturen definiert.
Weiters wird aber auch eine erste Anbindung an LLVM geschaffen und damit die Generierung des JIT-kompilierten Code ermöglicht.

### Operationen

Als nächster Schritt können die simplen Operationen (wie in der Spezifikation definiert) in LLVM IR abgebildet werden.

### Control Flow

Infolgedessen können danach einfache Control Flow Konstrukte wie Schleifen und If-Then-Else Konstrukte abgebildet werden, und somit ebenfalls JIT-kompiliert werden.

### Funktionsaufrufe

Abschließend sollen Funktionsaufrufe ermöglicht werden.
