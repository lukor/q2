# microbenchmark for instruction fusing

qqqqqqq.q # this function should optimize to a single (FArithmetic1)
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
    q.q
    q.q
    qqqqq.q
    qqqqq.q
qqqqqqqqq.q

qqqqqqq.q
    q.q  # 1 as accumulator

    q.qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
    qqq.q
    qqqqq.qqq # 1024*1024 as number of iterations

    qqqqqqq.q
        # swap call swap
        qqqq.q
        qqqqqq.q
        qqqq.q

        # decrement iterator by one
        q.q
        qqqqq.qq
    qqqqqqqq.qqq

    qq.q # pop the iterator
qqqqqqqq.q

# the result should be 33554433
