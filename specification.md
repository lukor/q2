# q

## Sprache

Die Programmiersprache q ist eine Stack-Sprache, welche das (wiederholte) Zeichen `q` als Instruktionen und beliebige andere Zeichen als Trennzeichen verwendet.
Die Wiederholung von `q` erlaubt die Unterscheidung zwischen verschiedenen Instruktionen und deren Parametern, welche nach der Anzahl an `q`s definiert sind.

q verwendet zwei Stacks, den expliziten Working-Stack auf dem Operationen Daten finden und ablegen können, und dem Control-Flow-Stack.


## Anweisungen

Anweisungen in q sind jeweils zwei Ketten von `q`s getrennt von einem beliebigen Trennzeichen (z.B.: `qq qqq`), wobei Anweisungen zwischeneinander ebenfalls von beliebigen Trennzeichen getrennt werden.
Eine Anweisung besteht hierbei aus dem Anweisungstyp gefolgt von dem Anweisungsparameter.

Anweisungen werden dabei in die Kategorien "Control-Flow" und "Operationen" unterschieden.

### Control-Flow Anweisungen

Control-Flow in q basiert auf der Definition und Ausführung von Blöcken.
Ein Block beginnt mit einer `StartBlock` Anweisung und wird mit entweder einer `DefineBlock` oder einer `Execute` Anweisung beendet.

Wenn eine `StartBlock` Anweisung von dem Interpreter eingelesen wird liest dieser so lange Anweisungen ein, bis die dazugehörige `DefineBlock` oder `Execute` Anweisung erreicht wird, ohne die enthaltenen Anweisungen auszuführen.
Es ist hierbei anzumerken, dass eine Verschachtelung von Blöcken möglich ist und der Interpreter den Beginn und das Ende enthaltener Blöcke gesondert behandelt, um das Ende des äußeren Blockes korrekt finden zu können, aber diese nicht ausführt oder für die Definition weiterer Blöcke nutzt, biss der äußere Block auch ausgeführt wird.

Wenn als Ende eines Blocks eine `Execute` Anweisung angefunden wird, werden die Anweisungen je nach Anweisungsparameter entweder bedingungslos ausgeführt, ausgeführt wenn er oberste Wert am Stack einer 1 entspricht (if), oder solange ausgeführt wie der oberste Wert am Stack einer 1 entspricht (while).

Wenn hingegen eine `DefineBlock` Anweisung erreicht wird, werden die Anweisungen als Blockdefinition abgelegt, um später durch einen Aufruf ausgeführt werden zu können.
Der Anweisungsparameter wird hierbei als Blockname verwendet, wenn bereits ein Block mit diesem Namen definiert ist wird dieser überschrieben. Einige Blöcke sind schon im Rahmen der Standardlibrary vordefiniert.

Um einen definierten Block auszuführen (sowohl User-definiert, als auch Blöcke aus der Standard-Library) werden diese mit der `Call` Anweisung aufgerufen, welche als Anweisungsparameter den Namen des auszuführenden Blocks nimmt.

### Operationen

Um Daten am Working-Stack bearbeiten zu können sind die folgenden Operationen definiert (`n` ist hierbei die Zahl die als Anweisungsparameter übergeben wird):

- Push: Setzt `n` als Zahl auf den Working-Stack
- Pop: Entfernt `n` Werte von dem Working-Stack
- Duplicate: Dupliziert den `n`ten Wert auf die oberste Stelle des Working-Stacks
- Swap: Tauscht den obersten Wert des Working-Stacks mit dem `n`ten Wert
- Arithmetic: Führt je nach Anweisungsparameter eine der folgenen Arithmetischen Operationen mit den obersten Werten (`..., a3, a2, a1, a0`) des Working-Stacks durch. Die verwendeten Werte werden dabei vom Working-Stack entfernt, das Ergebnis der Operation wird gepushed.
  - Add: `a1 + a0`
  - Subtract: `a1 - a0`
  - Multiply: `a1 * a0`
  - Divide: `a1 // a0`
  - Remainder: `a1 % a0`
