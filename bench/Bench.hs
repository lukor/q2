{-# OPTIONS_GHC -Wwarn=unused-imports #-}
module Bench where

import qualified Data.Map.Strict as Map

import Criterion.Main.Options
import Criterion.Main
import Criterion.Types

import IL

import qualified AppendTypeclassInterpreter as AT
import qualified AppendListInterpreter as AL
import qualified AppendADTInterpreter as AA
import qualified AppendMutableInterpreter as AM

import qualified ComposeListTranslatedInterpreter as CLT
import qualified ComposeADTInterpreter as CA
import qualified ComposeADTTranslatedInterpreter as CAT
import qualified ComposingMutableInterpreter as CM

import qualified ThreadingListInterpreter as TL
import qualified ThreadingADTInterpreter as TA
import qualified ThreadingMutableInterpreter as TM
import qualified ThreadingMutableFixedInterpreter as TMF


makeFib :: Int -> ([Instruction], Map.Map Int [Instruction])
makeFib x = ([(ExecuteBlock ([
                        (Push x),
                        (Call 1)
                    ], Always))],
                    Map.fromList [(1,[(ExecuteBlock ([
                        (Push 1),
                        (Arithmetic Subtraction),
                        (ExecuteBlock ([
                            (Duplicate 1),
                            (Call 1),
                            (Duplicate 2),
                            (Push 1),
                            (Arithmetic Subtraction),
                            (Call 1),
                            (Arithmetic Addition),
                            (Swap 1),
                            (Pop 1)
                        ], If)),
                        (Duplicate 1),
                        (Arithmetic Not),
                        (ExecuteBlock ([
                            (Pop 2),
                            (Push 1),
                            (Push 1)
                        ], If)),
                        (Pop 1)
                    ], If))])])

makeFibNonRec :: Int -> ([Instruction], Map.Map Int [Instruction])
makeFibNonRec x = ([ExecuteBlock ([
                            Push 1,
                            Push 1,
                            Push $ (x*x) - 2,
                            ExecuteBlock ([
                                Push 1,
                                Arithmetic Subtraction,
                                Swap 2,
                                Duplicate 2,
                                Arithmetic Addition,
                                Swap 1,
                                Swap 2
                            ], While),
                            Pop 1,
                            Swap 1,
                            Pop 1
                        ], Always)
                   ], mempty)

makeAddUp :: Int -> ([Instruction], Map.Map Int Instructions)
makeAddUp x = ([(ExecuteBlock (Push x : (concat $ replicate x [Push 1, Arithmetic Subtraction]), Always))], mempty)

makeLoop :: Int -> ([Instruction], Map.Map Int Instructions)
makeLoop x = ([ExecuteBlock ([Push x, ExecuteBlock ([Push 1, Arithmetic Subtraction], While)], Always)], mempty)

makeSubRec :: Int -> ([Instruction], Map.Map Int Instructions)
makeSubRec x = ([ExecuteBlock ([Push x, Call 1], Always)], Map.fromList [(1, [ExecuteBlock ([Push 1, Arithmetic Subtraction, Call 1], If)])])


myConfig :: Config
myConfig = defaultConfig {
            timeLimit = 30,
            reportFile = Just "criterion_results.html"
          }

fibSize :: Int
fibSize = 30

loopSize :: Int
loopSize = 1024

main :: IO ()
main = defaultMainWith myConfig [
    bgroup "fib recursive" [
           bgroup "Appending" [
               bench "Typeclass (List)"       $ nfIO (AT.run []    $ makeFib fibSize),
               bench "List"                   $ nfIO (AL.run       $ makeFib fibSize),
               bench "ADT"                    $ nfIO (AA.run       $ makeFib fibSize),
               bench "Mutable"                $ nfIO (AM.run       $ makeFib fibSize)
           ],
           bgroup "Composing" [
               bench "List (Translated)"      $ nfIO (CLT.run      $ makeFib fibSize),
               bench "ADT"                    $ nfIO (CA.run       $ makeFib fibSize),
               bench "ADT (Translated)"       $ nfIO (CAT.run      $ makeFib fibSize),
               bench "Mutable"                $ nfIO (CM.run       $ makeFib fibSize)
           ],
           bgroup "Threading" [
               bench "List"                   $ nfIO (TL.run       $ makeFib fibSize),
               bench "ADT"                    $ nfIO (TA.run       $ makeFib fibSize),
               bench "Mutable"                $ nfIO (TM.run       $ makeFib fibSize),
               bench "Mutable (Fixed)"        $ nfIO (TMF.run      $ makeFib fibSize)
           ]
    ],
    bgroup "fib non-recursive" [
           bgroup "Appending" [
               bench "Typeclass (List)"       $ nfIO (AT.run []    $ makeFibNonRec fibSize),
               bench "List"                   $ nfIO (AL.run       $ makeFibNonRec fibSize),
               bench "ADT"                    $ nfIO (AA.run       $ makeFibNonRec fibSize),
               bench "Mutable"                $ nfIO (AM.run       $ makeFibNonRec fibSize)
           ],
           bgroup "Composing" [
               bench "List (Translated)"      $ nfIO (CLT.run      $ makeFibNonRec fibSize),
               bench "ADT"                    $ nfIO (CA.run       $ makeFibNonRec fibSize),
               bench "ADT (Translated)"       $ nfIO (CAT.run      $ makeFibNonRec fibSize),
               bench "Mutable"                $ nfIO (CM.run       $ makeFibNonRec fibSize)
           ],
           bgroup "Threading" [
               bench "List"                   $ nfIO (TL.run       $ makeFibNonRec fibSize),
               bench "ADT"                    $ nfIO (TA.run       $ makeFibNonRec fibSize),
               bench "Mutable"                $ nfIO (TM.run       $ makeFibNonRec fibSize),
               bench "Mutable (Fixed)"        $ nfIO (TMF.run      $ makeFibNonRec fibSize)
           ]
    ],
    bgroup "subtract inline" [
           bgroup "Appending" [
               bench "Typeclass (List)"      $ nfIO (AT.run []    $ makeAddUp loopSize),
               bench "List"                  $ nfIO (AL.run       $ makeAddUp loopSize),
               bench "ADT"                   $ nfIO (AA.run       $ makeAddUp loopSize),
               bench "Mutable"               $ nfIO (AM.run       $ makeAddUp loopSize)
           ],
           bgroup "Composing" [
               bench "List (Translated)"     $ nfIO (CLT.run      $ makeAddUp loopSize),
               bench "ADT"                   $ nfIO (CA.run       $ makeAddUp loopSize),
               bench "ADT (Translated)"      $ nfIO (CAT.run      $ makeAddUp loopSize),
               bench "Mutable"               $ nfIO (CM.run       $ makeAddUp loopSize)
           ],
           bgroup "Threading" [
               bench "List"                  $ nfIO (TL.run       $ makeAddUp loopSize),
               bench "ADT"                   $ nfIO (TA.run       $ makeAddUp loopSize),
               bench "Mutable"               $ nfIO (TM.run       $ makeAddUp loopSize),
               bench "Mutable (Fixed)"       $ nfIO (TMF.run      $ makeAddUp loopSize)
           ]
    ],
    bgroup "subtract loop" [
           bgroup "Appending" [
               bench "Typeclass (List)"      $ nfIO (AT.run []    $ makeLoop loopSize),
               bench "List"                  $ nfIO (AL.run       $ makeLoop loopSize),
               bench "ADT"                   $ nfIO (AA.run       $ makeLoop loopSize),
               bench "Mutable"               $ nfIO (AM.run       $ makeLoop loopSize)
           ],
           bgroup "Composing" [
               bench "List (Translated)"     $ nfIO (CLT.run      $ makeLoop loopSize),
               bench "ADT"                   $ nfIO (CA.run       $ makeLoop loopSize),
               bench "ADT (Translated)"      $ nfIO (CAT.run      $ makeLoop loopSize),
               bench "Mutable"               $ nfIO (CM.run       $ makeLoop loopSize)
           ],
           bgroup "Threading" [
               bench "List"                  $ nfIO (TL.run       $ makeLoop loopSize),
               bench "ADT"                   $ nfIO (TA.run       $ makeLoop loopSize),
               bench "Mutable"               $ nfIO (TM.run       $ makeLoop loopSize),
               bench "Mutable (Fixed)"       $ nfIO (TMF.run      $ makeLoop loopSize)
           ]
    ],
    bgroup "subtract recursive" [
           bgroup "Appending" [
               bench "Typeclass (List)"      $ nfIO (AT.run []    $ makeSubRec loopSize),
               bench "List"                  $ nfIO (AL.run       $ makeSubRec loopSize),
               bench "ADT"                   $ nfIO (AA.run       $ makeSubRec loopSize),
               bench "Mutable"               $ nfIO (AM.run       $ makeSubRec loopSize)
           ],
           bgroup "Composing" [
               bench "List (Translated)"     $ nfIO (CLT.run      $ makeSubRec loopSize),
               bench "ADT"                   $ nfIO (CA.run       $ makeSubRec loopSize),
               bench "ADT (Translated)"      $ nfIO (CAT.run      $ makeSubRec loopSize),
               bench "Mutable"               $ nfIO (CM.run       $ makeSubRec loopSize)
           ],
           bgroup "Threading" [
               bench "List"                  $ nfIO (TL.run       $ makeSubRec loopSize),
               bench "ADT"                   $ nfIO (TA.run       $ makeSubRec loopSize),
               bench "Mutable"               $ nfIO (TM.run       $ makeSubRec loopSize),
               bench "Mutable (Fixed)"       $ nfIO (TMF.run      $ makeSubRec loopSize)
           ]
    ]
  ]
