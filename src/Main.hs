module Main where
import Frontend
import Options.Applicative

backendSelection :: Parser BackendConfig
-- list backend name twice for default
backendSelection = flag InterpreterBackend InterpreterBackend
                           ( long "interpreter"
                           <> short 'i'
                           <> help "Use interpreter backend (default)" )
                   <|> flag' JITBackend
                           ( long "jit"
                           <> short 'j'
                           <> help "Use JIT backend" )
                   <|> flag' DummyBackend
                           ( long "dummy"
                           <> short 'd'
                           <> help "Use dummy backend" )

programOptions :: Parser ProgramConfig
programOptions = ProgramConfig
                 <$> argument str (metavar "file")
                 <*> backendSelection


main :: IO ()
main = do
  start =<< execParser opts
      where opts = info (programOptions <**> helper)
              ( fullDesc
             <> progDesc "Run the given program with the specified backend"
             <> header "q" )
