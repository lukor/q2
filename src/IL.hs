{-# LANGUAGE DeriveGeneric #-}

module IL where

import GHC.Generics (Generic)
import Control.DeepSeq
type BlockName = Int

data ExecutionType = Always
                   | If
                   | While
                     deriving (Show, Eq, Generic)
instance NFData ExecutionType

data ArithmeticExpression = Addition
                          | Subtraction
                          | Multiplication
                          | Division
                          | Remainder
                          | And
                          | Or
                          | Not
                          | Equals
                          | LessThan
                            deriving (Show, Eq, Generic)
instance NFData ArithmeticExpression

data Instruction = Push(Int)
                 | Pop(Int)
                 | Duplicate(Int)
                 | Swap(Int)
                 | Arithmetic(ArithmeticExpression)
                 | ExecuteBlock(Instructions, ExecutionType)
                 | DefineBlock(Instructions, BlockName)
                 | Call(BlockName)
                 | PrintStack
                 -- internal instructions
                 | FArithmetic1(Int -> Int)
                 deriving Generic
instance NFData Instruction

type Instructions = [Instruction]

instance Show Instruction where
  show (Push         x         ) = "(Push " ++ show x ++ ")"
  show (Pop          x         ) = "(Pop " ++ show x ++ ")"
  show (Duplicate    x         ) = "(Duplicate " ++ show x ++ ")"
  show (Swap         x         ) = "(Swap " ++ show x ++ ")"
  show (Arithmetic   x         ) = "(Arithmetic " ++ show x ++ ")"
  show (ExecuteBlock(ins, et)  ) = "(ExecuteBlock (" ++ show ins ++ ", " ++ show et ++ "))"
  show (DefineBlock (ins, name)) = "(DefineBlock (" ++ show ins ++ ", " ++ show name ++ "))"
  show (Call         name      ) = "(Call " ++ show name ++ ")"
  show (PrintStack             ) = "(PrintStack)"
  show (FArithmetic1 _         ) = "(FArithmetic1)"

intifyBool :: Bool -> Int
intifyBool False = 0
intifyBool True = 1

boolifyInt :: Int -> Bool
boolifyInt 0 = False
boolifyInt _ = True

compareOp :: (Int -> Int -> Bool) -> (Int -> Int -> Int)
compareOp op a b = intifyBool $ op a b

logicOp :: (Bool -> Bool -> Bool) -> (Int -> Int -> Int)
logicOp op a b = intifyBool $ op (boolifyInt a) (boolifyInt b)

getBinaryOperator :: ArithmeticExpression -> (Int -> Int -> Int)
getBinaryOperator Addition       = (+)
getBinaryOperator Subtraction    = (-)
getBinaryOperator Multiplication = (*)
getBinaryOperator Division       = div
getBinaryOperator Remainder      = rem
getBinaryOperator And            = logicOp (&&)
getBinaryOperator Or             = logicOp (||)
getBinaryOperator Equals         = compareOp (==)
getBinaryOperator LessThan       = compareOp (<)
getBinaryOperator Not = error "getBinaryOperator should not be called with Not"
