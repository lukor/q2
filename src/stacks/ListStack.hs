{-# LANGUAGE TypeSynonymInstances, BangPatterns #-}
{-# OPTIONS_GHC -Wwarn=orphans #-}

module ListStack where

import Stack

type ListStack = []

instance Stack ListStack where
  push !item !items = item : items
  {-# INLINE push #-}

  dropN !n = drop n
  {-# INLINE dropN #-}

  peekAt !n !i = i !! n
  {-# INLINE peekAt #-}

  swapAt !n !i = content
    where
      (pre, suf) = splitAt n i
      content = head suf : tail pre ++ head pre : tail suf
  {-# INLINE swapAt #-}

  binaryOp !op (y:x:r) = op x y : r
  binaryOp _ _ = error "too few elements for binary operation"
  {-# INLINE binaryOp #-}

  unaryOp !op (x:r) = op x : r
  unaryOp _ _ = error "too few elements for unary operation"
  {-# INLINE unaryOp #-}

  test (x:_) = x /= 0
  test []    = error "too few elements for test"
  {-# INLINE test #-}
