module DataStackOld where

import Stack
data OldStack a = OldStack (OldStack a) a | EmptyOldStack

instance Show a => Show (OldStack a) where
    show EmptyOldStack = "EMPTY"
    show (OldStack EmptyOldStack n) = show n
    show (OldStack s n) = show s ++ ", " ++ show n

instance Semigroup (OldStack a) where
  (<>) EmptyOldStack s2 = s2
  (<>) (OldStack r a) s2 = OldStack ((<>) r s2) a

instance Monoid (OldStack a) where
  mempty = EmptyOldStack

instance Stack OldStack where
  push n s = OldStack s n
  {-# INLINE push #-}

  dropN 0 s = s
  dropN _ EmptyOldStack = error "Attempted to pop from an empty stack"
  dropN n (OldStack s _) = dropN (n-1) s
  {-# INLINE dropN #-}

  peekAt _ EmptyOldStack = error "Attempted to get a value which is not on the stack"
  peekAt 0 (OldStack _ x) = x
  peekAt n (OldStack s _) = peekAt (n-1) s
  {-# INLINE peekAt #-}

  swapAt n s  = (push . top) stail $ dropN 1 $ moveTop ((push . top) s $ dropN 1 stail) n s
    where stail = stackTail s n
  {-# INLINE swapAt #-}

  binaryOp _ EmptyOldStack = error "Attempted binary operation on empty stack"
  binaryOp _ (OldStack EmptyOldStack _) = error "Attempted binary operation on stack with only one element"
  binaryOp op (OldStack (OldStack rest l) r) = OldStack rest $ op l r
  {-# INLINE binaryOp #-}

  unaryOp _ EmptyOldStack = error "Attempted unary operation on empty stack"
  unaryOp op (OldStack rest v) = OldStack rest $ op v
  {-# INLINE unaryOp #-}

  test EmptyOldStack = error "Tested for condition on empty stack"
  test (OldStack _ v) = v /= 0
  {-# INLINE test #-}

top :: OldStack a -> a
top = peekAt 1
{-# INLINE top #-}

moveTop :: OldStack a -> Int -> OldStack a -> OldStack a
moveTop s 0 _ = s
moveTop _ _ EmptyOldStack = error "Attempted to get value which is not on the stack"
moveTop b n (OldStack s x) = OldStack (moveTop b (n-1) s) x
{-# INLINE moveTop #-}

-- stackHead :: Int -> DataStackOld a -> DataStackOld a
-- stackHead = moveTop EmptyOldStack

stackTail :: OldStack a -> Int -> OldStack a
stackTail s 0 = s
stackTail EmptyOldStack _ = error "Attempted to get value which is not on the stack"
stackTail (OldStack s _) n = stackTail s $ n-1
{-# INLINE stackTail #-}
