{-# LANGUAGE TypeSynonymInstances, BangPatterns #-}

module Stack where

class Stack s where
  push :: a -> s a -> s a
  dropN :: Int -> s a -> s a
  peekAt :: Int -> s a -> a
  swapAt :: Int -> s a -> s a
  binaryOp :: (a -> a -> a) -> s a -> s a
  unaryOp :: (a -> a) -> s a -> s a
  test :: (Eq a, Num a) => s a -> Bool
