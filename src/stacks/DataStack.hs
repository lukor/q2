{-# LANGUAGE DeriveGeneric #-}

module DataStack where

import Stack
import GHC.Generics (Generic)
import Control.DeepSeq

data CrazyStack a = a :> CrazyStack a
             | EmptyStack
             deriving (Show, Generic)
instance NFData a => NFData (CrazyStack a)

instance Semigroup (CrazyStack a) where
  (<>) EmptyStack s2 = s2
  (<>) (a :> r) s2 = a :> ((<>) r s2)

instance Monoid (CrazyStack a) where
  mempty = EmptyStack

instance Stack CrazyStack where
  push x r = x :> r
  {-# INLINE push #-}

  dropN 0 s = s
  dropN _ EmptyStack = error "dropping too much"
  dropN n (a :> r) = dropN (n-1) r
  {-# INLINE dropN #-}

  peekAt _ EmptyStack = error "peeking too far"
  peekAt 0 (x :> _) = x
  peekAt n (x :> r) = peekAt (n-1) r
  {-# INLINE peekAt #-}

  swapAt _ EmptyStack = error "swapping in empty stack"
  swapAt 1 (x :> (y :> r)) = y :> (x :> r)
  {-# INLINE swapAt #-}


  binaryOp op (y :> (x :> r)) = op x y :> r
  binaryOp _ _ = error "too few elements"
  {-# INLINE binaryOp #-}

  unaryOp op (x :> r) = op x :> r
  {-# INLINE unaryOp #-}

  test (x :> _) = x /= 0
  {-# INLINE test #-}
