{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE OverloadedStrings #-}

module JITBackend (run) where

import JITIL as IL
import Helpers

import Data.Text.Lazy.IO as T
import qualified Data.Map.Strict as Map
import qualified GHC.Word
import Data.Maybe
import Control.Monad.Fix

import LLVM.Pretty

import qualified LLVM.AST as AST
import LLVM.AST.Constant
import LLVM.AST.Name
import LLVM.AST.Type
import LLVM.AST.Operand
import qualified LLVM.AST.IntegerPredicate as IP

import LLVM.Module
import LLVM.Context

import LLVM.IRBuilder.Module
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Instruction

import qualified LLVM.ExecutionEngine as EE

import Foreign.Ptr ( FunPtr, castFunPtr )
foreign import ccall "dynamic" haskFun :: FunPtr (IO ()) -> (IO ())

type GlobalMap = Map.Map String Operand

-- frame, stack, elements in current frame, current frame length, frame ptr
type StackState = (Operand, Operand, Operand, Operand, Operand)

-- no type aliases (yet) - see https://github.com/llvm-hs/llvm-hs/issues/295 (merged, but not released)
t_stackEntry :: Type
t_stackEntry = i32
constOp :: Integer -> Operand
constOp i = ConstantOperand $ Int 32 i
-- base ptr, stack ptr, frame size, next frame, prev frame
t_stackFrame :: Type
t_stackFrame = StructureType True [ptr t_stackEntry, ptr t_stackEntry, i32, ptr i8, ptr i8]

initialStackSize :: Integer
initialStackSize = 4
growSizeStrategy :: MonadIRBuilder m => Operand -> m Operand
growSizeStrategy = return
numElemsToI8 :: MonadIRBuilder m => Operand -> m Operand
numElemsToI8 = mul $ ConstantOperand $ sizeof t_stackEntry

updMap :: MonadModuleBuilder m => String -> m a -> Map.Map String a -> m (Map.Map String a)
updMap k v m = v >>= (\uv -> return $ Map.insert k uv m)

insertValueMulti :: MonadIRBuilder m => [Operand] -> [GHC.Word.Word32] -> Operand -> m Operand
insertValueMulti vals positions target = foldl (>>=) (return target) $ map (\(v,p) t -> insertValue t v [p]) $ zip vals positions

run :: ([Instruction], Map.Map Integer [Instruction]) -> IO ()
run (instructions, functions) = r (unwrapsFuncs functions) instructions
  where
    unwrapsFuncs :: Map.Map Integer [Instruction] -> [Instruction]
    unwrapsFuncs funcs = map (\(k, v) -> DefineBlock(v, k)) $ Map.assocs funcs
    r :: [Instruction] -> [Instruction] -> IO()
    r code mainBlks = runFn $ buildModule "q" $ do
                        funcs <- extFuncDecls
                        glbls <- globalsDecls
                        stdlib <- genStdlib glbls funcs
                        codegen code mainBlks glbls stdlib

runFn :: AST.Module -> IO ()
runFn = _runJIT

_runPrintIR :: AST.Module -> IO ()
_runPrintIR llvmMod = T.putStrLn $ ppllvm llvmMod

jit :: Context -> (EE.MCJIT -> IO a) -> IO a
jit c = EE.withMCJIT c optlevel model ptrelim fastins
  where
    optlevel = Just 2  -- optimization level
    model    = Nothing -- code model ( Default )
    ptrelim  = Nothing -- frame pointer elimination
    fastins  = Nothing -- fast instruction selection

_runJIT :: AST.Module -> IO ()
_runJIT llvmMod = withContext $ \context ->
             jit context $ \engine ->
                 withModuleFromAST context llvmMod $ \m ->
                     EE.withModuleInEngine engine m $ \ee -> do
                       mainfn <- EE.getFunction ee (mkName "main")
                       haskFun (castFunPtr $ fromJust mainfn :: FunPtr (IO ()))

initialMap :: MonadModuleBuilder m => Map.Map String a -> m (Map.Map String a)
initialMap = return

extFuncDecls :: MonadModuleBuilder m => m GlobalMap
extFuncDecls = initialMap Map.empty
               >>= updMap "malloc" (extern "malloc" [i32] $ ptr i8)
               >>= updMap "free" (extern "free" [ptr i8] void)
               >>= updMap "printf" (externVarArgs "printf" [ptr i8] i32)
               >>= updMap "exit" (extern "exit" [i32] void)

globalsDecls :: MonadModuleBuilder m => m GlobalMap
globalsDecls = initialMap Map.empty
               >>= updMap "stackCurrentFrame" (global "stackCurrentFrame" (ptr t_stackFrame) (Null $ ptr t_stackFrame))
               >>= updMap "stackPrintFStr"    (globalStringPtr "%d, " "stackPrintFStr" >>= return . ConstantOperand)
               >>= updMap "stackEndNl"        (globalStringPtr "END\n" "stackEndNl" >>= return . ConstantOperand)
               >>= updMap "stackOverflow"     (globalStringPtr "FATAL: Could not allocate memory for the stack\n" "stackOverflow" >>= return . ConstantOperand)
               >>= updMap "stackEmpty"        (globalStringPtr "FATAL: Tried to %s an element which is not on the stack\n" "stackEmpty" >>= return . ConstantOperand)
               >>= updMap "pop"               (globalStringPtr "pop" "pop" >>= return . ConstantOperand)
               >>= updMap "dup"               (globalStringPtr "duplicate" "dup" >>= return . ConstantOperand)
               >>= updMap "swp"               (globalStringPtr "swap" "swp" >>= return . ConstantOperand)
               >>= updMap "add"               (globalStringPtr "add" "add" >>= return . ConstantOperand)
               >>= updMap "if"                (globalStringPtr "if" "if" >>= return . ConstantOperand)
               >>= updMap "while"             (globalStringPtr "while" "while" >>= return . ConstantOperand)
               >>= updMap "strFStr"           (globalStringPtr "%s\n" "strFStr" >>= return . ConstantOperand)
               >>= updMap "strlen"            (globalStringPtr "strlen" "strlen" >>= return . ConstantOperand)

genStdlib :: (MonadFix m, MonadModuleBuilder m) => GlobalMap -> GlobalMap -> m GlobalMap
genStdlib glbls funcs = mdo
  strlen <- function "strln" [] i32 $ \[] -> mdo
                          (frame, stack, numElems, frameSize, framePtr) <- loadStackState glbls
                          br beforeLoop
                          beforeLoop <- block
                          br loopStart
                          loopStart <- block
                          len <- phi [(constOp 0, beforeLoop), (incLen, loopFoot)]
                          nowFrame <- phi [(frame, beforeLoop), (newFrame, loopFoot)]
                          nowStack <- phi [(stack, beforeLoop), (newStack, loopFoot)]
                          nowNumElems <- phi [(numElems, beforeLoop), (newNumElems, loopFoot)]
                          nowFrameSize <- phi [(frameSize, beforeLoop), (newFrameSize, loopFoot)]
                          nowFramePtr <- phi [(framePtr, beforeLoop), (newFramePtr, loopFoot)]
                          incLen <- add len $ constOp 1
                          (newFrame, newStack, newNumElems, newFrameSize, newFramePtr) <- stackSeek (mapEntry "strlen" glbls) glbls funcs 1 (nowFrame, nowStack, nowNumElems, nowFrameSize, nowFramePtr)
                          br loopFoot
                          loopFoot <- block
                          newStackI8 <- bitcast newStack $ ptr i8
                          chr <- load newStackI8 0
                          stringEnd <- icmp IP.EQ chr $ ConstantOperand $ Int 8 0
                          condBr stringEnd loopEnd loopStart
                          loopEnd <- block
                          ret len
  printf <- function "fun7" [] void $ \[] -> mdo
                          (frame, stack, numElems, frameSize, framePtr) <- loadStackState glbls
                          strln <- call strlen []
                          strlnnull <- add strln $ constOp 1
                          strbuf <- call (mapEntry "malloc" funcs) [(strlnnull, [])]
                          
                          br beforeLoop
                          beforeLoop <- block
                          br loopStart
                          loopStart <- block
                          nowBuf <- phi [(strbuf, beforeLoop), (newBuf, loopFoot)]
                          nowFrame <- phi [(frame, beforeLoop), (newFrame, loopFoot)]
                          nowStack <- phi [(stack, beforeLoop), (newStack, loopFoot)]
                          nowNumElems <- phi [(numElems, beforeLoop), (newNumElems, loopFoot)]
                          nowFrameSize <- phi [(frameSize, beforeLoop), (newFrameSize, loopFoot)]
                          nowFramePtr <- phi [(framePtr, beforeLoop), (newFramePtr, loopFoot)]
                          (newFrame, newStack, newNumElems, newFrameSize, newFramePtr) <- stackSeek (mapEntry "strlen" glbls) glbls funcs 1 (nowFrame, nowStack, nowNumElems, nowFrameSize, nowFramePtr)
                          br loopFoot
                          loopFoot <- block
                          newStackI8 <- bitcast newStack $ ptr i8
                          chr <- load newStackI8 0
                          store nowBuf 0 chr

                          newBuf <- gep nowBuf [constOp 1]
                          stringEnd <- icmp IP.EQ chr $ ConstantOperand $ Int 8 0
                          condBr stringEnd loopEnd loopStart
                          loopEnd <- block

                          _ <- call (mapEntry "printf" funcs) [(mapEntry "strFStr" glbls, []), (strbuf, [])]
                          _ <- call (mapEntry "free" funcs) [(strbuf, [])]
                          return ()
  return $ Map.insert "fun7" printf funcs


codegen :: (MonadFix m, MonadModuleBuilder m) => [Instruction] -> [Instruction] -> GlobalMap -> GlobalMap -> m GlobalMap
codegen (DefineBlock(body, bname):blks) mainBlks glbls funcs = mdo
  fun <- blkCodeGen glbls allFuncs body $ "fun" ++ show bname
  allFuncs <- codegen blks mainBlks glbls $ Map.insert ("fun" ++ show bname) fun funcs
  return allFuncs
codegen (_:_) _ _ _ = error "Primitive instruction / block on function level"
codegen [] mainBlks glbls funcs = genMain mainBlks glbls funcs >> return funcs


blkCodeGen :: (MonadFix m, MonadModuleBuilder m) => GlobalMap -> GlobalMap -> [Instruction] -> String -> m Operand
blkCodeGen glbls funcs body bname = function (mkName bname) [] void $ \[] ->
                                    (foldl (>>=) (loadStackState glbls) $ map (renderInstruction glbls funcs) body) >>=
                                    storeStackState glbls >> retVoid


genMain :: (MonadFix m, MonadModuleBuilder m) => [Instruction] -> GlobalMap -> GlobalMap -> m Operand
genMain body glbls funcs = function "main" [] void $ \[] -> do
                                       endState <- foldl (>>=) (stackSetup glbls funcs) $ map (renderInstruction glbls funcs) body
                                       stackTeardown glbls funcs endState
                                       retVoid

stackSetup :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> m StackState
stackSetup glbls funcs = mdo
  mallocSize <- numElemsToI8 $ constOp initialStackSize
  stackI8 <- call (mapEntry "malloc" funcs) [(mallocSize, [])]
  stackFramePtrI8 <- call (mapEntry "malloc" funcs) [(ConstantOperand $ sizeof $ t_stackFrame, [])]
  stackAllocated <- icmp IP.NE stackI8 $ ConstantOperand $ Null $ ptr i8
  frameAllocated <- icmp IP.NE stackFramePtrI8 $ ConstantOperand $ Null $ ptr i8
  bothAllocated <- LLVM.IRBuilder.Instruction.and stackAllocated frameAllocated
  condBr bothAllocated allocateFrame allocateFail
  allocateFrame <- block
  stack <- bitcast stackI8 $ ptr t_stackEntry
  stackFramePtr <- bitcast stackFramePtrI8 $ ptr t_stackFrame
  stackFrame <- load stackFramePtr 0
  stackFrameMod <- insertValueMulti [stack, stack, constOp initialStackSize, ConstantOperand $ Null $ ptr i8, ConstantOperand $ Null $ ptr i8] [0, 1, 2, 3, 4] stackFrame
  store stackFramePtr 0 stackFrameMod
  store (mapEntry "stackCurrentFrame" glbls) 0 stackFramePtr
  br rest
  allocateFail <- block
  _ <- call (mapEntry "printf" funcs) [(mapEntry "stackOverflow" glbls, [])]
  _ <- call (mapEntry "exit" funcs) [(constOp 1, [])]
  rest <- block
  return (stackFrameMod, stack, constOp 0, constOp initialStackSize, stackFramePtr)

stackTeardown :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> StackState -> m ()
stackTeardown glbls funcs (stackFrame, stack, numElems, frameLen, framePtr) = mdo
  br startTeardown
  startTeardown <- block
  -- TODO remove
  _ <- printStack glbls funcs (stackFrame, stack, numElems, frameLen, framePtr)
  framePtrI8 <- bitcast framePtr $ ptr i8
  br startDeallocate
  startDeallocate <- block
  -- deallocate higher than current frames (kept allocated for later use)
  br deallocateBack
  deallocateBack <- block
  currFrameBack <- phi [(stackFrame, startDeallocate), (nextFrameBack, doDeallocateBack)]
  nextFrameBackPtrI8 <- extractValue currFrameBack [4]
  endReachedBack <- icmp IP.EQ nextFrameBackPtrI8 $ ConstantOperand $ Null $ ptr i8
  condBr endReachedBack deallocateFront doDeallocateBack
  doDeallocateBack <- block
  nextFrameBackPtr <- bitcast nextFrameBackPtrI8 $ ptr t_stackFrame
  nextFrameBack <- load nextFrameBackPtr 0
  stackBack <- extractValue nextFrameBack [0]
  stackBackI8 <- bitcast stackBack $ ptr i8
  _ <- call (mapEntry "free" funcs) [(stackBackI8, [])]
  _ <- call (mapEntry "free" funcs) [(nextFrameBackPtrI8, [])]
  br deallocateBack
  -- deallocate current and lower frames
  deallocateFront <- block
  currFrameFrontPtr <- phi [(framePtrI8, deallocateBack), (nextFrameFrontPtrI8, doDeallocateFront)]
  currFrameFront <- phi [(stackFrame, deallocateBack), (nextFrameFront, doDeallocateFront)]
  nextFrameFrontPtrI8 <- extractValue currFrameFront [3]
  endReachedFront <- icmp IP.EQ nextFrameFrontPtrI8 $ ConstantOperand $ Null $ ptr i8
  condBr endReachedFront deallocateDone doDeallocateFront
  doDeallocateFront <- block
  nextFrameFrontPtr <- bitcast nextFrameFrontPtrI8 $ ptr t_stackFrame
  nextFrameFront <- load nextFrameFrontPtr 0
  stackFront <- extractValue currFrameFront [0]
  stackFrontI8 <- bitcast stackFront $ ptr i8
  _ <- call (mapEntry "free" funcs) [(stackFrontI8, [])]
  _ <- call (mapEntry "free" funcs) [(currFrameFrontPtr, [])]
  br deallocateFront
  deallocateDone <- block
  -- dealloc first frame
  firstStackFrame <- extractValue currFrameFront [0]
  firstStackFrameI8 <- bitcast firstStackFrame $ ptr i8
  _ <- call (mapEntry "free" funcs) [(firstStackFrameI8, [])]
  _ <- call (mapEntry "free" funcs) [(currFrameFrontPtr, [])]
  return ()


renderInstruction :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> Instruction -> StackState -> m StackState
renderInstruction glbls funcs (Push n) = push glbls funcs $ constOp n
renderInstruction glbls funcs (Pop n) = pop glbls funcs n
renderInstruction glbls funcs (Duplicate n) = dup glbls funcs n
renderInstruction glbls funcs (Swap n) = swap glbls funcs n
renderInstruction glbls funcs (Arithmetic op) = arith glbls funcs op
renderInstruction glbls funcs (Call n) = callFun glbls funcs n
renderInstruction glbls funcs (ExecuteBlock (body, Always)) = \s -> foldl (>>=) (return s) $ map (renderInstruction glbls funcs) body
renderInstruction glbls funcs (ExecuteBlock (body, If)) = execIf glbls funcs body
renderInstruction glbls funcs (ExecuteBlock (body, While)) = execWhile glbls funcs body
renderInstruction glbls funcs PrintStack = printStack glbls funcs
renderInstruction _ _ (DefineBlock _) = error "Nested functions are not supported"
renderInstruction _ _ (FArithmetic1 _) = error "Optimizations not implemented"

execIf :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> [Instruction] -> StackState -> m StackState
execIf glbls funcs body state@(startFrame, startStack, startNum, startSize, startPtr) = mdo
  (_, stack, _, _, _) <- stackSeek (mapEntry "if" glbls) glbls funcs 1 state
  cond <- load stack 0
  skip <- icmp IP.EQ cond $ constOp 0
  br ifStart
  ifStart <- block
  condBr skip ifEnd ifBody
  ifBody <- block
  (modFrame, modStack, modNum, modSize, modPtr) <- renderInstruction glbls funcs (ExecuteBlock (body, Always)) state
  br ifVisited
  ifVisited <- block
  br ifEnd
  ifEnd <- block
  endFrame <- phi [(startFrame, ifStart), (modFrame, ifVisited)]
  endStack <- phi [(startStack, ifStart), (modStack, ifVisited)]
  endNum <- phi [(startNum, ifStart), (modNum, ifVisited)]
  endSize <- phi [(startSize, ifStart), (modSize, ifVisited)]
  endPtr <- phi [(startPtr, ifStart), (modPtr, ifVisited)]
  return (endFrame, endStack, endNum, endSize, endPtr)

execWhile :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> [Instruction] -> StackState -> m StackState
execWhile glbls funcs body state@(startFrame, startStack, startNum, startSize, startPtr) = mdo
  br whileStart
  whileStart <- block
  br whileCond
  whileCond <- block
  endFrame <- phi [(startFrame, whileStart), (modFrame, whileVisited)]
  endStack <- phi [(startStack, whileStart), (modStack, whileVisited)]
  endNum <- phi [(startNum, whileStart), (modNum, whileVisited)]
  endSize <- phi [(startSize, whileStart), (modSize, whileVisited)]
  endPtr <- phi [(startPtr, whileStart), (modPtr, whileVisited)]
  (_, stack, _, _, _) <- stackSeek (mapEntry "while" glbls) glbls funcs 1 (endFrame, endStack, endNum, endSize, endPtr)
  cond <- load stack 0
  done <- icmp IP.EQ cond $ constOp 0
  condBr done whileEnd whileBody
  whileBody <- block
  (modFrame, modStack, modNum, modSize, modPtr) <- renderInstruction glbls funcs (ExecuteBlock (body, Always)) state
  br whileVisited
  whileVisited <- block
  br whileCond
  whileEnd <- block
  return (endFrame, endStack, endNum, endSize, endPtr)

push :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> Operand -> StackState -> m StackState
push glbls funcs val startState@(frame, stack, numElems, frameSize, framePtr) = mdo
  br pushStart
  pushStart <- block
  frameFull <- icmp IP.EQ numElems frameSize
  condBr frameFull grow doPush
  grow <- block
  (grownFrame, grownStack, _, grownFrameSize, grownFramePtr) <- growStack glbls funcs startState
  br growDone
  growDone <- block
  br doPush
  doPush <- block

  actualFrame <- phi [(frame, pushStart), (grownFrame, growDone)]
  actualStack <- phi [(stack, pushStart), (grownStack, growDone)]
  actualNumElems <- phi [(numElems, pushStart), (constOp 0, growDone)]
  actualFrameSize <- phi [(frameSize, pushStart), (grownFrameSize, growDone)]
  actualFramePtr <- phi [(framePtr, pushStart), (grownFramePtr, growDone)]

  store actualStack 0 val
  newSp <- gep actualStack [constOp 1]
  newNumElems <- add actualNumElems $ constOp 1
  return (actualFrame, newSp, newNumElems, actualFrameSize, actualFramePtr)

pop :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> Integer -> StackState -> m StackState
pop glbls = stackSeek (mapEntry "pop" glbls) glbls

dup :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> Integer -> StackState -> m StackState
dup glbls funcs n state = mdo
  (_, ptr, _, _, _) <- stackSeek (mapEntry "dup" glbls) glbls funcs n state
  val <- load ptr 0
  push glbls funcs val state

swap :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> Integer -> StackState -> m StackState
swap glbls funcs n state = mdo
  (_, ptr1, _, _, _) <- stackSeek (mapEntry "swp" glbls) glbls funcs 1 state
  (_, ptr2, _, _, _) <- stackSeek (mapEntry "swp" glbls) glbls funcs (n+1) state
  val1 <- load ptr1 0
  val2 <- load ptr2 0
  store ptr1 0 val2
  store ptr2 0 val1
  return state

arith :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> ArithmeticExpression -> StackState -> m StackState
arith glbls funcs Not state = mdo
  (_, targetPtr, _, _, _) <- stackSeek (mapEntry "add" glbls) glbls funcs 1 state
  prevVal <- load targetPtr 0
  newValBool <- icmp IP.EQ prevVal $ constOp 0
  newVal <- select newValBool (constOp 1) (constOp 0)
  store targetPtr 0 newVal
  return state
arith glbls funcs exprType state = do
  (op1Frame, op1Ptr, op1numEl, op1FrameSize, op1FramePtr) <- stackSeek (mapEntry "add" glbls) glbls funcs 1 state
  (retFrame, op2Ptr, numElems, retFrameSize, retFramePtr) <- stackSeek (mapEntry "add" glbls) glbls funcs 1 (op1Frame, op1Ptr, op1numEl, op1FrameSize, op1FramePtr)
  op1 <- load op1Ptr 0
  op2 <- load op2Ptr 0
  res <- toLlvmOp exprType op2 op1
  store op2Ptr 0 res
  retStack <- gep op2Ptr [constOp 1]
  retNumElems <- add numElems $ constOp 1
  return (retFrame, retStack, retNumElems, retFrameSize, retFramePtr)
    where toLlvmOp Addition = add
          toLlvmOp Subtraction = sub
          toLlvmOp Multiplication = mul
          toLlvmOp Division = sdiv
          toLlvmOp Remainder = srem
          toLlvmOp IL.And = LLVM.IRBuilder.Instruction.and
          toLlvmOp IL.Or = LLVM.IRBuilder.Instruction.or
          toLlvmOp Equals = boolOp $ icmp IP.EQ
          toLlvmOp LessThan = boolOp $ icmp IP.SLT
          toLlvmOp Not = error "This should not happen"
          boolOp op x y = do
            res <- op x y
            select res (constOp 1) (constOp 0)


callFun :: MonadIRBuilder m => GlobalMap -> GlobalMap -> Integer -> StackState -> m StackState
callFun glbls funcs fid s = case Map.lookup ("fun" ++ show fid) funcs of
                              Just fun -> do
                                storeStackState glbls s
                                _ <- call fun []
                                newState <- loadStackState glbls
                                return newState
                              Nothing -> error $ "Call to undefined function " ++ show fid


printStack :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> StackState -> m StackState
printStack glbls funcs stackState@(frame, _, numElems, _, _) = mdo
  br loopHeader
  loopHeader <- block
  br loopStart
  loopStart <- block
  currFrame <- phi [(frame, loopHeader), (nextFrame, loadNextFrame)]
  currFrameSize <- phi [(numElems, loopHeader), (nextFrameSize, loadNextFrame)]
  bp <- extractValue currFrame [0]
  stackStart <- gep bp [currFrameSize]
  br innerLoopStart
  -- print single frame
  innerLoopStart <- block
  sp <- phi [(stackStart, loopStart), (nextElem, innerLoopBody)]
  endReached <- icmp IP.EQ sp bp
  condBr endReached innerLoopEnd innerLoopBody
  innerLoopBody <- block
  nextElem <- gep sp [constOp (-1)]
  el <- load nextElem 0
  _ <- call (mapEntry "printf" funcs) [(mapEntry "stackPrintFStr" glbls, []), (el, [])]
  br innerLoopStart
  innerLoopEnd <- block
  -- move to next frame
  nextFramePtrI8 <- extractValue currFrame [3]
  stackEndReached <- icmp IP.EQ nextFramePtrI8 $ ConstantOperand $ Null $ ptr i8
  condBr stackEndReached loopEnd loadNextFrame
  loadNextFrame <- block
  nextFramePtr <- bitcast nextFramePtrI8 $ ptr t_stackFrame
  nextFrame <- load nextFramePtr 0
  nextFrameSize <- extractValue nextFrame [2]
  br loopStart
  loopEnd <- block
  -- done with all frames
  _ <- call (mapEntry "printf" funcs) [(mapEntry "stackEndNl" glbls, [])]
  return stackState

stackSeek :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => Operand -> GlobalMap -> GlobalMap -> Integer -> StackState -> m StackState
stackSeek _ _ _ 0 s = return s
stackSeek action glbls funcs n (frame, stack, numElems, frameSize, framePtr) = mdo
  br seekStart
  seekStart <- block
  br seekMore
  seekMore <- block
  toSkip <- phi [(constOp (-n), seekStart), (leftoverElems, moveStack)]
  currFrame <- phi [(frame, seekStart), (nextFrame, moveStack)]
  currStack <- phi [(stack, seekStart), (nextStack, moveStack)]
  currElems <- phi [(numElems, seekStart), (frameLen, moveStack)]
  currFrameLen <- phi [(frameSize, seekStart), (frameLen, moveStack)]
  currFramePtr <- phi [(framePtr, seekStart), (nextFramePtr, moveStack)]
  leftoverElems <- add currElems toSkip -- toSkip is negative
  notInFrame <- icmp IP.SLT leftoverElems $ constOp 0
  condBr notInFrame moveFrame seekEnd
  moveFrame <- block
  -- frame exceeded, move to next frame
  nextFrameI8 <- extractValue currFrame [3]
  nextFramePtr <- bitcast nextFrameI8 $ ptr t_stackFrame
  nextFramePtrInt <- ptrtoint nextFramePtr i32
  nextFrameExists <- icmp IP.UGT nextFramePtrInt $ constOp 0
  condBr nextFrameExists moveStack stackEndReached
  moveStack <- block
  nextFrame <- load nextFramePtr 0
  nextStackStart <- extractValue nextFrame [0]
  frameLen <- extractValue nextFrame [2]
  nextStack <- gep nextStackStart [frameLen]
  br seekMore
  -- stack end reached
  stackEndReached <- block
  _ <- call (mapEntry "printf" funcs) [(mapEntry "stackEmpty" glbls, []), (action, [])]
  _ <- call (mapEntry "exit" funcs) [(constOp 1, [])]
  br seekEnd
  seekEnd <- block
  -- finally seek in current frame
  newSp <- gep currStack [toSkip]
  return (currFrame, newSp, leftoverElems, currFrameLen, currFramePtr)

growStack :: (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m) => GlobalMap -> GlobalMap -> StackState -> m StackState
growStack glbls funcs (frame, _, _, frameSize, framePtr) = mdo
  existingNextPtrI8 <- extractValue frame [4]
  hasNextFrame <- icmp IP.UGT existingNextPtrI8 $ ConstantOperand $ Null $ ptr i8
  condBr hasNextFrame resetFrame allocateFrameMalloc
  -- reuse previously allocated frame
  resetFrame <- block
  reusedFramePtr <- bitcast existingNextPtrI8 $ ptr t_stackFrame
  reusedFramePrev <- load reusedFramePtr 0
  reusedStack <- extractValue reusedFramePrev [0]
  reusedFrame <- insertValue reusedFramePrev reusedStack [1]
  reusedSize <- extractValue reusedFrame [2]
  br growEnd
  -- allocate new frame
  allocateFrameMalloc <- block
  allocSize <- growSizeStrategy frameSize
  mallocSize <- numElemsToI8 allocSize
  allocStackI8 <- call (mapEntry "malloc" funcs) [(mallocSize, [])]
  allocFramePtrI8 <- call (mapEntry "malloc" funcs) [(ConstantOperand $ sizeof $ t_stackFrame, [])]
  stackAllocated <- icmp IP.NE allocStackI8 $ ConstantOperand $ Null $ ptr i8
  frameAllocated <- icmp IP.NE allocFramePtrI8 $ ConstantOperand $ Null $ ptr i8
  bothAllocated <- LLVM.IRBuilder.Instruction.and stackAllocated frameAllocated
  condBr bothAllocated allocateFrame allocateFail
  allocateFrame <- block
  allocStack <- bitcast allocStackI8 $ ptr t_stackEntry
  allocFramePtr <- bitcast allocFramePtrI8 $ ptr t_stackFrame
  allocFrame <- load allocFramePtr 0
  currPtrI8 <- bitcast framePtr $ ptr i8
  allocFrameMod <- insertValueMulti [allocStack, allocStack, allocSize, currPtrI8, ConstantOperand $ Null $ ptr i8] [0, 1, 2, 3, 4] allocFrame
  lastFrame <- insertValue frame allocFramePtrI8 [4]
  store framePtr 0 lastFrame
  store allocFramePtr 0 allocFrameMod
  br growEnd
  allocateFail <- block
  _ <- call (mapEntry "printf" funcs) [(mapEntry "stackOverflow" glbls, [])]
  _ <- call (mapEntry "exit" funcs) [(constOp 1, [])]
  growEnd <- block
  nowFrame <- phi [(allocFrameMod, allocateFrame), (reusedFrame, resetFrame)]
  nowStack <- phi [(allocStack, allocateFrame), (reusedStack, resetFrame)]
  nowSize <- phi [(allocSize, allocateFrame), (reusedSize, resetFrame)]
  nowFramePtr <- phi [(allocFramePtr, allocateFrame), (reusedFramePtr, resetFrame)]
  return (nowFrame, nowStack, constOp 0, nowSize, nowFramePtr)

loadStackState :: MonadIRBuilder m => GlobalMap -> m StackState
loadStackState glbls = do
  stackFramePtr <- load (mapEntry "stackCurrentFrame" glbls) 0
  stackFrame <- load stackFramePtr 0
  stack <- extractValue stackFrame [1]
  base <- extractValue stackFrame [0]
  spInt <- ptrtoint stack i32
  baseInt <- ptrtoint base i32
  numElemsPtr <- sub spInt baseInt
  numElems <- udiv numElemsPtr $ ConstantOperand $ sizeof $ t_stackEntry
  frameLen <- extractValue stackFrame [2]
  return (stackFrame, stack, numElems, frameLen, stackFramePtr)

storeStackState :: (MonadIRBuilder m) => GlobalMap -> StackState -> m ()
storeStackState glbls (frame, stack, _, _, stackFramePtr) = do
  newFrame <- insertValue frame stack [1]
  store stackFramePtr 0 newFrame
  store (mapEntry "stackCurrentFrame" glbls) 0 stackFramePtr
