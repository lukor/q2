module Optimizations where

import IL

-- for optimizations to be able to combine newly optimized instructions with previously emitted instructions we have two
-- separate stacks, whereby the optimization stack is in reverse and therefore matches from the most recent instruction
-- backwards.
-- it is important that the pattern match first matches onto the optimizations of the optimization stack, before adding
-- adding new instructions from the input stack or returning the results, since eg. there may still be optimizations
-- possible, even if no new instructions are available on the input stack
constantPropagation :: Instructions -> Instructions
constantPropagation = cp []
  where
    cp :: Instructions -> Instructions -> Instructions
    -- optimization cases
    cp (Arithmetic Not : Push n : rest)         res = cp (Push (if n == 0 then 1 else 0) : rest) res
    cp (Arithmetic op : Push m : Push n : rest) res = cp (Push (getBinaryOperator op n m) : rest) res
    -- default cases
    cp st []                             = reverse st
    cp st (ExecuteBlock(insts, et) : xs) = cp (ExecuteBlock(constantPropagation insts, et) : st) xs
    cp st (x:xs)                         = cp (x : st) xs

instructionFusing :: Instructions -> Instructions
instructionFusing = inf []
  where
    inf :: Instructions -> Instructions -> Instructions
    -- optimization cases
    inf (Arithmetic op : Push n : rest)            res = inf (FArithmetic1 ((flip $ getBinaryOperator op) n) : rest) res
    inf (FArithmetic1 o2 : FArithmetic1 o1 : rest) res = inf (FArithmetic1 (o2 . o1) : rest) res
    -- default cases
    inf st []                             = reverse st
    inf st (ExecuteBlock(insts, et) : xs) = inf (ExecuteBlock(instructionFusing insts, et) : st) xs
    inf st (x:xs)                         = inf (x : st) xs
