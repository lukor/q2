module AppendListInterpreter (run) where

import IL
import qualified Data.Map.Strict as Map

type Stack a = [a]

newtype Blocks = Blocks { unpackBlocks :: Map.Map Int (Instructions -> Stack Int -> Blocks -> IO ()) }

run :: (Instructions, Map.Map Int Instructions) -> IO ()
run (instructions, blocks) = eval instructions mempty lib
  where
    lib = Blocks $ Map.map mapping blocks
    mapping body remaining = eval (body ++ remaining)

-- execute instructions on a stack
eval :: Instructions -> Stack Int -> Blocks -> IO ()
eval (  (ExecuteBlock(body, Always)):rest) s bs = eval (body ++ rest) s bs
eval (  (ExecuteBlock(body, If))    :rest) s@(x:_) bs = if x /= 0 then eval (body ++ rest) s bs else eval rest s bs
eval (b@(ExecuteBlock(body, While)) :rest) s@(x:_) bs = if x /= 0 then eval (body ++ (b:rest)) s bs else eval rest s bs
eval (  (DefineBlock(body, name))   :rest) s bs = eval rest s $ Blocks $ Map.insert name (\remaining -> eval (body ++ remaining)) (unpackBlocks bs)
-- call
eval ((Call name):rest) s bs = body rest s bs
  where
    body = getBlock name (unpackBlocks bs)
    getBlock bname blocks = case Map.lookup bname blocks of
      Nothing -> error $ "Trying to call unknown block " ++ show bname
      Just content -> content
eval (PrintStack:rest) s bs = do print $ "Stack: " ++ show s; eval rest s bs
-- stack operations
eval (op:rest) s bs = eval rest (mutateStack op s) bs
eval [] s _ = print $ "Stack: " ++ show s

mutateStack :: Instruction -> Stack Int -> Stack Int
mutateStack (Push n)      s = n : s
mutateStack (Pop n)       s = drop n s
mutateStack (Duplicate n) s = (s !! (n-1)) : s
mutateStack (Swap 1)      (x:y:s) = y:x:s
mutateStack (Swap 2)      (a:x:y:s) = y:x:a:s

mutateStack (Arithmetic   Not) (x:s) = (if x == 0 then 1 else 0) : s
mutateStack (Arithmetic   op)  (y:x:s) = getBinaryOperator op x y : s
mutateStack (FArithmetic1 op)  (x:s) = op x : s

mutateStack i s = error $ "problem with instruction " ++ show i ++ " on stack " ++ show s
