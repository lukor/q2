{-# LANGUAGE DeriveGeneric #-}

module ComposeADTInterpreter (run) where

import GHC.Generics (Generic)
import Control.DeepSeq
import qualified Data.Map.Strict as Map
import IL

data Stack a = !a :> !(Stack a)
             | EmptyStack
             deriving (Show, Generic)
instance NFData a => NFData (Stack a)

instance Semigroup (Stack a) where
  (<>) EmptyStack s2 = s2
  (<>) (a :> r) s2 = a :> ((<>) r s2)

instance Monoid (Stack a) where
  mempty = EmptyStack

push :: a -> Stack a -> Stack a
push x r = x :> r

dropN :: Int -> Stack a -> Stack a
dropN 0 s = s
dropN _ EmptyStack = error "dropping too much"
dropN n (_ :> r) = dropN (n-1) r

peekAt :: Int -> Stack a -> a
peekAt _ EmptyStack = error "peeking too far"
peekAt 0 (x :> _) = x
peekAt n (_ :> r) = peekAt (n-1) r

swapAt :: (Show a) => Int -> Stack a -> Stack a
swapAt n       EmptyStack         = error $ "swapping " ++ show n ++ " in empty stack"
swapAt n       (_ :> EmptyStack)  = error $ "swapping " ++ show n ++ " in 1 element stack"
swapAt 1       (x :> (y :> r))    = y :> (x :> r)
swapAt 2 (a :> (x :> (y :> r)))   = y :> (x :> (a :> r))
swapAt n (a :> r) = (peekAt (n-1) r) :> (swapAt' (n-1) a r)
    where swapAt' :: Int -> a -> Stack a -> Stack a
          swapAt' 0 b (_ :> s)   = b :> s
          swapAt' m b (x :> s)   = x :> (swapAt' (m-1) b s)
          swapAt' _ _ EmptyStack = error "stack too small for swap"

test :: (Eq a, Num a) => Stack a -> Bool
test EmptyStack = error "can not test an empty stack"
test (x :> _) = x /= 0

newtype Blocks = Blocks { unpackBlocks :: Map.Map Int (Blocks -> Stack Int -> IO (Stack Int))}

run :: (Instructions, Map.Map Int Instructions) -> IO (Stack Int)
run (instructions, blocks) = do rs <- eval lib instructions mempty; print $ "Stack: " ++ show rs; return rs
    where
        lib = Blocks (Map.map mapping blocks)
        mapping body bs = eval bs body

-- execute instructions on a stack
eval :: Blocks -> Instructions -> Stack Int -> IO (Stack Int)
eval bs (  (ExecuteBlock(body, Always)):rest) s = eval bs body s                                                >>= eval bs rest
eval bs (  (ExecuteBlock(body, If))    :rest) s = (if test s then eval bs body s                 else return s) >>= eval bs rest
eval bs (i@(ExecuteBlock(body, While)) :rest) s = (if test s then eval bs body s >>= eval bs [i] else return s) >>= eval bs rest

-- call
eval bs ((Call name):rest) s = body bs s >>= eval bs rest
  where
    body = getBlock name (unpackBlocks bs)
    getBlock bname blocks = case Map.lookup bname blocks of
      Nothing -> error $ "Trying to call unknown block " ++ show bname
      Just content -> content
eval bs (PrintStack:rest) s = do print $ "Stack: " ++ show s; eval bs rest s

-- stack operations
eval bs ((Push      n):rest) s = eval bs rest $ n :> s
eval bs ((Pop       n):rest) s = eval bs rest $ dropN n s
eval bs ((Duplicate n):rest) s = eval bs rest $ push (peekAt (n-1) s) s
eval bs ((Swap      n):rest) s = eval bs rest $ swapAt n s

eval bs ((Arithmetic   Not):rest)       (0 :> s)  = eval bs rest $ 1 :> s
eval bs ((Arithmetic   Not):rest)       (_ :> s)  = eval bs rest $ 0 :> s
eval bs ((Arithmetic   op ):rest) (x :> (y :> r)) = eval bs rest $ getBinaryOperator op y x :> r
eval bs ((FArithmetic1 op ):rest)       (a :> s)  = eval bs rest $ op a :> s

eval _ ((Arithmetic   Not):_) EmptyStack        = error "can not invert an empty stack"
eval _ ((FArithmetic1 _  ):_) EmptyStack        = error "can not operate on an empty stack"
eval _ ((Arithmetic   _  ):_) EmptyStack        = error "can not operate on an empty stack"
eval _ ((Arithmetic   _  ):_) (_ :> EmptyStack) = error "can not operate on an empty stack"
eval _ ((DefineBlock  _  ):_) _                 = error "define block where it should not be"

eval _ [] s = return s
