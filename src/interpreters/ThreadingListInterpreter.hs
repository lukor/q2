{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE BangPatterns #-}

module ThreadingListInterpreter (run) where

import qualified IL as IL
import qualified Data.IntMap.Strict as Map
import qualified Data.Map.Strict as MMap



--------------------------------
-- Instructions
--------------------------------

type Stack = [Int]
type Instruction = Stack -> IO (Stack)
type Blocks = Map.IntMap (Instruction -> Instruction)



--------------------------------
-- helper functions
--------------------------------

test :: Stack -> Bool
test [] = error "can not read from empty stack"
test (0:_) = False
test _    = True
{-# INLINE test #-}

lo :: Int -> Map.IntMap a -> a
lo n bs = check $ Map.lookup n bs
                where
                    check Nothing = error $ "unknown block with name " ++ show n
                    check (Just a) = a
{-# INLINE lo #-}


--------------------------------
-- actions
--------------------------------

push :: Int -> Instruction -> Instruction
push n r s = r $ n : s

pop :: Int -> Instruction -> Instruction
pop n r s = r $ drop n s

duplicate :: Int -> Instruction -> Instruction
duplicate n r s = r $ (s !! (n-1)) : s

swap :: Int -> Instruction -> Instruction
swap 1 r   (x:y:s) = r $ y:x:s
swap 2 r (a:x:y:s) = r $ y:x:a:s
swap n _ _ = error $ "swap for level " ++ show n ++ " not implemented for current interpreter"

invert :: Instruction -> Instruction
invert _ [] = error "can not invert empty stack"
invert r (0:a) = r $ 1:a
invert r (_:a) = r $ 0:a

arithmetic :: (Int -> Int -> Int) -> Instruction -> Instruction
arithmetic op r (y:x:s) = r $ op x y : s
arithmetic _ _ _ = error "stack too small for arithmetic"

printStack :: Instruction -> Instruction
printStack r s = do print $ show s; r s

call :: Blocks -> Int -> Instruction -> Instruction
call bs name r s = lo name bs r s

executeAlways :: (Instruction -> Instruction) -> Instruction -> Instruction
executeAlways b r s = b r s

executeIf :: (Instruction -> Instruction) -> Instruction -> Instruction
executeIf b r s = if test s then b r s else r s

executeWhile :: (Instruction -> Instruction) -> Instruction -> Instruction
executeWhile b r s = if test s then b (\ns -> executeWhile b r ns) s else r s



--------------------------------
-- translation
--------------------------------

translate :: Blocks -> IL.Instruction -> Instruction -> Instruction
translate _  (IL.Push      n) = push      n
translate _  (IL.Pop       n) = pop       n
translate _  (IL.Duplicate n) = duplicate n
translate _  (IL.Swap      n) = swap      n

translate _  (IL.Arithmetic   IL.Not) = invert
translate _  (IL.Arithmetic   i     ) = arithmetic   (IL.getBinaryOperator i)
-- translate _  (IL.FArithmetic1 i     ) = fArithmetic1 i

translate bs (IL.ExecuteBlock (is, IL.Always)) = executeAlways $ combine $ map (translate bs) is
translate bs (IL.ExecuteBlock (is, IL.If    )) = executeIf     $ combine $ map (translate bs) is
translate bs (IL.ExecuteBlock (is, IL.While )) = executeWhile  $ combine $ map (translate bs) is

translate bs (IL.Call name) = call bs name

translate _  IL.PrintStack = printStack

translate _ x = error $ "can not translate " ++ show x


--------------------------------
-- run
--------------------------------

run :: (IL.Instructions, MMap.Map Int IL.Instructions) -> IO [Int]
run (instructions, definitions) = do
    let blocks = Map.fromList $ map (\(k,is) -> (k, combine $ map (translate blocks) is)) $ MMap.toList definitions
    s <- combine (map (translate blocks) instructions) return []
    print $ "Stack: " ++ show s
    return s

combine :: [Instruction -> Instruction] -> Instruction -> Instruction
combine [] r = r
combine (x:xs) r = x $ combine xs r
{-# INLINE combine #-}
