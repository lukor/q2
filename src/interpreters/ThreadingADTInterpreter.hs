{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE BangPatterns #-}

module ThreadingADTInterpreter (run) where

import qualified IL as IL
import qualified Data.IntMap.Strict as Map
import qualified Data.Map.Strict as MMap



--------------------------------
-- Instructions
--------------------------------

data Stack = !Int :> !Stack
             | EmptyStack
             deriving (Show)

instance Semigroup Stack where
  (<>) EmptyStack s2 = s2
  (<>) (a :> r) s2 = a :> ((<>) r s2)

instance Monoid Stack where
  mempty = EmptyStack

type Instruction = Stack -> IO (Stack)
type Blocks = Map.IntMap (Instruction -> Instruction)



--------------------------------
-- helper functions
--------------------------------

test :: Stack -> Bool
test EmptyStack = error "can not read from empty stack"
test (0:>_) = False
test _    = True
{-# INLINE test #-}

lo :: Int -> Map.IntMap a -> a
lo n bs = check $ Map.lookup n bs
                where
                    check Nothing = error $ "unknown block with name " ++ show n
                    check (Just a) = a
{-# INLINE lo #-}

dropN :: Int -> Stack -> Stack
dropN 0 s = s
dropN _ EmptyStack = error "dropping too much"
dropN n (_ :> r) = dropN (n-1) r
{-# INLINE dropN #-}

peekAt :: Int -> Stack -> Int
peekAt _ EmptyStack = error "peeking too far"
peekAt 0 (x :> _) = x
peekAt n (_ :> r) = peekAt (n-1) r
{-# INLINE peekAt #-}

swapAt :: Int -> Stack -> Stack
swapAt n       EmptyStack         = error $ "swapping " ++ show n ++ " in empty stack"
swapAt n       (_ :> EmptyStack)  = error $ "swapping " ++ show n ++ " in 1 element stack"
swapAt 1       (x :> (y :> r))    = y :> (x :> r)
swapAt 2 (a :> (x :> (y :> r)))   = y :> (x :> (a :> r))
swapAt n (a :> r) = (peekAt (n-1) r) :> (swapAt' (n-1) a r)
    where swapAt' :: Int -> Int -> Stack -> Stack
          swapAt' 0 b (_ :> s)   = b :> s
          swapAt' m b (x :> s)   = x :> (swapAt' (m-1) b s)
          swapAt' _ _ EmptyStack = error "stack too small for swap"
{-# INLINE swapAt #-}


--------------------------------
-- actions
--------------------------------

push :: Int -> Instruction -> Instruction
push n r s = r $ n :> s

pop :: Int -> Instruction -> Instruction
pop n r s = r $ dropN n s

duplicate :: Int -> Instruction -> Instruction
duplicate n r s = r $ peekAt (n-1) s :> s

swap :: Int -> Instruction -> Instruction
swap n r s = r $ swapAt n s

invert :: Instruction -> Instruction
invert _ EmptyStack = error "can not invert empty stack"
invert r (0:>a) = r $ 1:>a
invert r (_:>a) = r $ 0:>a

arithmetic :: (Int -> Int -> Int) -> Instruction -> Instruction
arithmetic op r (y:>(x:>s)) = r $ op x y :> s
arithmetic _ _ _ = error "stack too small for arithmetic"

printStack :: Instruction -> Instruction
printStack r s = do print $ show s; r s

call :: Blocks -> Int -> Instruction -> Instruction
call bs name r s = lo name bs r s

executeAlways :: (Instruction -> Instruction) -> Instruction -> Instruction
executeAlways b r s = b r s

executeIf :: (Instruction -> Instruction) -> Instruction -> Instruction
executeIf b r s = if test s then b r s else r s

executeWhile :: (Instruction -> Instruction) -> Instruction -> Instruction
executeWhile b r s = if test s then b (\ns -> executeWhile b r ns) s else r s



--------------------------------
-- translation
--------------------------------

translate :: Blocks -> IL.Instruction -> Instruction -> Instruction
translate _  (IL.Push      n) = push      n
translate _  (IL.Pop       n) = pop       n
translate _  (IL.Duplicate n) = duplicate n
translate _  (IL.Swap      n) = swap      n

translate _  (IL.Arithmetic   IL.Not) = invert
translate _  (IL.Arithmetic   i     ) = arithmetic   (IL.getBinaryOperator i)
-- translate _  (IL.FArithmetic1 i     ) = fArithmetic1 i

translate bs (IL.ExecuteBlock (is, IL.Always)) = executeAlways $ combine $ map (translate bs) is
translate bs (IL.ExecuteBlock (is, IL.If    )) = executeIf     $ combine $ map (translate bs) is
translate bs (IL.ExecuteBlock (is, IL.While )) = executeWhile  $ combine $ map (translate bs) is

translate bs (IL.Call name) = call bs name

translate _  IL.PrintStack = printStack

translate _ x = error $ "can not translate " ++ show x


--------------------------------
-- run
--------------------------------

run :: (IL.Instructions, MMap.Map Int IL.Instructions) -> IO ()
run (instructions, definitions) = do
    let blocks = Map.fromList $ map (\(k,is) -> (k, combine $ map (translate blocks) is)) $ MMap.toList definitions
    s <- combine (map (translate blocks) instructions) return mempty
    print $ "Stack: " ++ show s

combine :: [Instruction -> Instruction] -> Instruction -> Instruction
combine [] r = r
combine (x:xs) r = x $ combine xs r
{-# INLINE combine #-}
