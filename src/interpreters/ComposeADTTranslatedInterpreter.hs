{-# LANGUAGE DeriveGeneric, BangPatterns #-}

module ComposeADTTranslatedInterpreter (run, translate, exec, Instruction, Blocks(..)) where

import GHC.Generics (Generic)
import Control.Monad
import Control.DeepSeq
import qualified Data.Map.Strict as Map
import qualified IL



--------------------------------
-- Stack
--------------------------------

data Stack a = !a :> !(Stack a)
             | EmptyStack
             deriving (Show, Generic)
instance NFData a => NFData (Stack a)

instance Semigroup (Stack a) where
  (<>) EmptyStack s2 = s2
  (<>) (a :> r) s2 = a :> ((<>) r s2)

instance Monoid (Stack a) where
  mempty = EmptyStack



--------------------------------
-- Instructions
--------------------------------

type Instruction a = Stack a -> Blocks a -> IO (Stack a)
type Instructions a = [Instruction a]

newtype Blocks a = Blocks { unpackBlocks :: Map.Map IL.BlockName (Instructions a) }



--------------------------------
-- helper functions
--------------------------------

peekAt :: Int -> Stack a -> a
peekAt _ EmptyStack = error "peeking too far"
peekAt 0 (x :> _) = x
peekAt n (_ :> r) = peekAt (n-1) r

dropN :: Int -> Stack a -> Stack a
dropN 0 s = s
dropN _ EmptyStack = error "dropping too much"
dropN n (_ :> r) = dropN (n-1) r

swapAt :: (Show a) => Int -> Stack a -> Stack a
swapAt n       EmptyStack         = error $ "swapping " ++ show n ++ " in empty stack"
swapAt n       (_ :> EmptyStack)  = error $ "swapping " ++ show n ++ " in 1 element stack"
swapAt 1       (x :> (y :> r))    = y :> (x :> r)
swapAt 2 (a :> (x :> (y :> r)))   = y :> (x :> (a :> r))
swapAt n (a :> r) = (peekAt (n-1) r) :> (swapAt' (n-1) a r)
    where swapAt' :: Int -> a -> Stack a -> Stack a
          swapAt' 0 b (_ :> s)   = b :> s
          swapAt' m b (x :> s)   = x :> (swapAt' (m-1) b s)
          swapAt' _ _ EmptyStack = error "stack too small for swap"

invert' :: (Eq a, Num a) => Stack a -> Stack a
invert' EmptyStack = error "not not damn"
invert' (0 :> s) = 1 :> s
invert' (_ :> s) = 0 :> s


--------------------------------
-- actions
--------------------------------

push :: a -> Instruction a
push x s _ = return $ x :> s

pop :: Int -> Instruction a
pop c s _ = return $ dropN c s

duplicate :: Int -> Instruction a
duplicate n s bs = push (peekAt (n-1) s) s bs

swap :: (Show a) => Int -> Instruction a
swap n s _ = return $ swapAt n s

invert :: (Eq a, Num a) => Instruction a
invert s _ = return $ invert' s

arithmetic :: (a -> a -> a) -> Instruction a
arithmetic op (x :> (y :> s)) _ = return $ op y x :> s
arithmetic _ _ _ = error "not arithmetic"

fArithmetic1 :: (a -> a) -> Instruction a
fArithmetic1 _ EmptyStack _ = error "not farithmetic1"
fArithmetic1 op (a :> s) _ = return $ op a :> s

executeAlways :: [Instruction a] -> Instruction a
executeAlways is s bs    = exec is s bs

executeIf :: (Eq a, Num a) => [Instruction a] -> Instruction a
executeIf _  EmptyStack _  = error "bad if"
executeIf _  s@(0 :> _) _  = return s
executeIf is s          bs = exec is s bs

executeWhile :: (Eq a, Num a) => [Instruction a] -> Instruction a
executeWhile _  EmptyStack _ = error "bad while"
executeWhile _ s@(0 :> _) _ = return s
executeWhile is s bs         = do r <- exec is s bs; executeWhile is r bs

call :: IL.BlockName -> Instruction a
call name s bs = exec is s bs
  where is = case Map.lookup name (unpackBlocks bs) of
          Nothing -> error $ "Trying to call unknown block " ++ show name
          Just content -> content

printStack :: (Show a) => Instruction a
printStack s _ = do print $ "Stack: " ++ show s; return s



--------------------------------
-- translation
--------------------------------

translate :: IL.Instruction -> Instruction Int
translate (IL.Push      n) = push      n
translate (IL.Pop       n) = pop       n
translate (IL.Duplicate n) = duplicate n
translate (IL.Swap      n) = swap      n

translate (IL.Arithmetic   IL.Not) = invert
translate (IL.Arithmetic   i     ) = arithmetic   (IL.getBinaryOperator i)
translate (IL.FArithmetic1 i     ) = fArithmetic1 i

translate (IL.ExecuteBlock (is, IL.Always)) = executeAlways $ map translate is
translate (IL.ExecuteBlock (is, IL.If    )) = executeIf     $ map translate is
translate (IL.ExecuteBlock (is, IL.While )) = executeWhile  $ map translate is

translate (IL.Call name) = call name
translate IL.PrintStack = printStack

translate i = error $ "unknown instruction can not translate " ++ show i



--------------------------------
-- run
--------------------------------

run :: (IL.Instructions, Map.Map IL.BlockName IL.Instructions) -> IO (Stack Int)
run (instructions, blocks) = do r <- exec is mempty lib; print $ "Stack: " ++ show r; return r
  where is = map translate instructions
        lib = Blocks (Map.map mapping blocks)
          where mapping :: IL.Instructions -> [Instruction Int]
                mapping body = map translate body

exec :: Instructions a -> Instruction a
exec !is !s !bs = concatM (map (\i s' -> i s' bs) is) s

concatM :: [a -> IO a] -> (a -> IO a)
concatM fs = foldr (>=>) return fs
