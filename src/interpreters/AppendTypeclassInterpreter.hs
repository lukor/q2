{-# LANGUAGE RankNTypes #-}

module AppendTypeclassInterpreter (run, eval, mutateStack) where

import IL
import Stack
import ListStack
import qualified Data.Map.Strict as Map

newtype Blocks = Blocks { unpackBlocks :: forall s . (Stack s, Show (s Int), Monoid (s Int)) => Map.Map Int (Instructions -> s Int -> Blocks -> IO ()) }

{-# SPECIALISE run :: ListStack Int -> (Instructions, Map.Map Int Instructions) -> IO () #-}
run :: (Stack s, Show (s Int), Monoid (s Int)) => s Int -> (Instructions, Map.Map Int Instructions) -> IO ()
run stack (instructions, blocks) = eval instructions stack lib
  where
    lib = Blocks $ Map.map mapping blocks
    mapping body remaining = eval (body ++ remaining)

-- execute instructions on a stack
{-# SPECIALISE eval :: Instructions -> ListStack Int -> Blocks -> IO () #-}
eval :: (Stack s, Show (s Int), Monoid (s Int)) => Instructions -> s Int -> Blocks -> IO ()
eval (  (ExecuteBlock(body, Always)):rest) s bs = eval (body ++ rest) s bs
eval (  (ExecuteBlock(body, If))    :rest) s bs = if test s then eval (body ++ rest) s bs else eval rest s bs
eval (b@(ExecuteBlock(body, While)) :rest) s bs = if test s then eval (body ++ (b:rest)) s bs else eval rest s bs
eval (  (DefineBlock(body, name))   :rest) s bs = eval rest s $ Blocks $ Map.insert name (\remaining -> eval (body ++ remaining)) (unpackBlocks bs)
-- call
eval ((Call name):rest) s bs = body rest s bs
  where
    body = getBlock name (unpackBlocks bs)
    getBlock bname blocks = case Map.lookup bname blocks of
      Nothing -> error $ "Trying to call unknown block " ++ show bname
      Just content -> content
eval (PrintStack:rest) s bs = do print $ "Stack: " ++ show s; eval rest s bs
-- stack operations
eval (op:rest) s bs = eval rest (mutateStack op s) bs
eval [] s _ = print $ "Stack: " ++ show s

{-# SPECIALISE mutateStack :: Instruction -> ListStack Int -> ListStack Int #-}
mutateStack :: (Stack s, Show (s Int), Monoid (s Int)) => Instruction -> s Int -> s Int
mutateStack (Push n)      s = push n s
mutateStack (Pop n)       s = dropN n s
mutateStack (Duplicate n) s = push (peekAt (n-1) s) s
mutateStack (Swap n)      s = swapAt n s

mutateStack (Arithmetic   Not) s = unaryOp (\x -> if x == 0 then 1 else 0) s
mutateStack (Arithmetic   op)  s = binaryOp (getBinaryOperator op) s
mutateStack (FArithmetic1 op)  s = unaryOp op s

mutateStack _ s = s
