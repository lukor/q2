{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE BangPatterns #-}

module ThreadingMutableFixedInterpreter (run) where

import qualified IL as IL
import Control.Monad
import Control.Monad.IO.Class
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as M
import qualified Data.IntMap.Strict as Map
import qualified Data.Map.Strict as MMap



--------------------------------
-- Types
--------------------------------

type Stack = M.IOVector Int
type Instruction = Int -> IO Int
type Blocks = Map.IntMap Instruction



--------------------------------
-- helper functions
--------------------------------

test :: Stack -> Int -> IO Bool
test v p = do x <- M.read v p; return (x /= 0)

printMV :: M.Unbox a => Show a => M.IOVector a -> Int -> IO ()
printMV v p = do
    fv <- V.freeze v
    liftIO $ print $ show p ++ " " ++ (show $ take p $ drop 1 (V.toList fv))

lo :: Int -> Map.IntMap a -> a
lo n bs = check $ Map.lookup n bs
                where
                    check Nothing = error $ "unknown block with name " ++ show n
                    check (Just a) = a


--------------------------------
-- actions
--------------------------------

push :: Int -> Stack -> Instruction
push n v p = do M.write v np n; return np
                    where np = p+1

pop :: Int -> Instruction
pop n p = return $ p-n

duplicate :: Int -> Stack -> Instruction
duplicate n v p = do x <- M.read v (p-n+1); M.write v np x; return np
                        where np = p+1

swap :: Int -> Stack -> Instruction
swap n v p = do M.swap v p (p-n); return p

invert :: Stack -> Instruction
invert v p = do M.modify v (\x -> if x == 0 then 1 else 0) p; return p

arithmetic :: (Int -> Int -> Int) -> Stack -> Instruction
arithmetic op v p = do y <- M.read v p; M.modify v (\x -> x `op` y) (p-1); return $ p-1

executeAlways :: Instruction -> Instruction
executeAlways b ps = b ps

executeIf :: Stack -> Instruction -> Instruction
executeIf v b p = do t <- test v p
                     if t then b p else return p

executeWhile :: Stack -> Instruction -> Instruction
executeWhile v b p = do t <- test v p
                        if t then do np <- b p; executeWhile v b np else return p

call :: Blocks -> Int -> Instruction
call bs name p = lo name bs p

printStack :: Stack -> Instruction
printStack v p = do printMV v p; return p



--------------------------------
-- translation
--------------------------------

translate :: Stack -> Blocks -> IL.Instruction -> Instruction
translate s _  (IL.Push      n) = push      n s
translate _ _  (IL.Pop       n) = pop       n
translate s _  (IL.Duplicate n) = duplicate n s
translate s _  (IL.Swap      n) = swap      n s

translate s _  (IL.Arithmetic   IL.Not) = invert s
translate s _  (IL.Arithmetic   i     ) = arithmetic (IL.getBinaryOperator i) s

translate s bs (IL.ExecuteBlock (is, IL.Always)) = executeAlways   $ concatM $ map (translate s bs) is
translate s bs (IL.ExecuteBlock (is, IL.If    )) = executeIf     s $ concatM $ map (translate s bs) is
translate s bs (IL.ExecuteBlock (is, IL.While )) = executeWhile  s $ concatM $ map (translate s bs) is

translate _ bs (IL.Call name) = call bs name

translate s _  IL.PrintStack = printStack s

translate _ _ x = error $ "can not translate " ++ show x


--------------------------------
-- run
--------------------------------

run :: (IL.Instructions, MMap.Map Int IL.Instructions) -> IO [Int]
run (instructions, definitions) = do
  vec <- do
    stack <- M.new 1024
    let blocks = Map.fromList $ map (\(k,is) -> (k, concatM $ map (translate stack blocks) is)) $ MMap.toList definitions
    p <- concatM (map (translate stack blocks) instructions) 0
    printMV stack p
    V.freeze $ M.take p $ M.drop 1 stack
  return $ V.toList vec

concatM :: [a -> IO a] -> (a -> IO a)
concatM fs = foldr (>=>) return fs
