{-# LANGUAGE DeriveGeneric, BangPatterns #-}

module ComposeListTranslatedInterpreter (run, translate, exec, Instruction, Blocks(..)) where

import Control.Monad
import qualified Data.Map.Strict as Map
import qualified IL



--------------------------------
-- Stack
--------------------------------

type Stack = [Int]


--------------------------------
-- Instructions
--------------------------------

type Instruction = Stack -> Blocks -> IO Stack
type Instructions = [Instruction]

newtype Blocks = Blocks { unpackBlocks :: Map.Map IL.BlockName Instructions }


--------------------------------
-- actions
--------------------------------

push :: Int -> Instruction
push x s _ = return $ x : s

pop :: Int -> Instruction
pop c s _ = return $ drop c s

duplicate :: Int -> Instruction
duplicate n s _ = return $ (s !! (n-1)) : s

swap :: Int -> Instruction
swap 1   (x:y:s) _ = return $ y:x:s
swap 2 (a:x:y:s) _ = return $ y:x:a:s
swap n         _ _ = error $ "swap not implemented for " ++ show n ++ " in current interpreter"

invert :: Instruction
invert (0 : s) _ = return $ 1 : s
invert (_ : s) _ = return $ 0 : s
invert []      _= error "can not invert on empty stack"

arithmetic :: (Int -> Int -> Int) -> Instruction
arithmetic op (x : (y : s)) _ = return $ op y x : s
arithmetic _ _ _ = error "not arithmetic"

fArithmetic1 :: (Int -> Int) -> Instruction
fArithmetic1 _ []       _ = error "not farithmetic1"
fArithmetic1 op (a : s) _ = return $ op a : s

executeAlways :: [Instruction] -> Instruction
executeAlways is s bs    = exec is s bs

executeIf :: [Instruction] -> Instruction
executeIf _  []        _  = error "bad if"
executeIf _  s@(0 : _) _  = return s
executeIf is s          bs = exec is s bs

executeWhile :: [Instruction] -> Instruction
executeWhile _  []       _ = error "bad while"
executeWhile _ s@(0 : _) _ = return s
executeWhile is s bs         = do r <- exec is s bs; executeWhile is r bs

call :: IL.BlockName -> Instruction
call name s bs = exec is s bs
  where is = case Map.lookup name (unpackBlocks bs) of
          Nothing -> error $ "Trying to call unknown block " ++ show name
          Just content -> content

printStack :: Instruction
printStack s _ = do print $ "Stack: " ++ show s; return s



--------------------------------
-- translation
--------------------------------

translate :: IL.Instruction -> Instruction
translate (IL.Push      n) = push      n
translate (IL.Pop       n) = pop       n
translate (IL.Duplicate n) = duplicate n
translate (IL.Swap      n) = swap      n

translate (IL.Arithmetic   IL.Not) = invert
translate (IL.Arithmetic   i     ) = arithmetic   (IL.getBinaryOperator i)
translate (IL.FArithmetic1 i     ) = fArithmetic1 i

translate (IL.ExecuteBlock (is, IL.Always)) = executeAlways $ map translate is
translate (IL.ExecuteBlock (is, IL.If    )) = executeIf     $ map translate is
translate (IL.ExecuteBlock (is, IL.While )) = executeWhile  $ map translate is

translate (IL.Call name) = call name
translate IL.PrintStack = printStack

translate i = error $ "unknown instruction can not translate " ++ show i



--------------------------------
-- run
--------------------------------

run :: (IL.Instructions, Map.Map IL.BlockName IL.Instructions) -> IO (Stack)
run (instructions, blocks) = do r <- exec is mempty lib; print $ "Stack: " ++ show r; return r
  where is = map translate instructions
        lib = Blocks (Map.map mapping blocks)
          where mapping :: IL.Instructions -> [Instruction]
                mapping body = map translate body

exec :: Instructions -> Instruction
exec !is !s !bs = concatM (map (\i s' -> i s' bs) is) s

concatM :: [a -> IO a] -> (a -> IO a)
concatM fs = foldr (>=>) return fs
