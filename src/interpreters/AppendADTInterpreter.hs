{-# LANGUAGE DeriveGeneric #-}

module AppendADTInterpreter(run) where

import IL
import Control.DeepSeq
import qualified Data.Map.Strict as Map
import GHC.Generics (Generic)

data Stack a = !a :> !(Stack a)
             | EmptyStack
             deriving (Show, Generic)
instance NFData a => NFData (Stack a)

instance Semigroup (Stack a) where
  (<>) EmptyStack s2 = s2
  (<>) (a :> r) s2 = a :> ((<>) r s2)

instance Monoid (Stack a) where
  mempty = EmptyStack

push :: a -> Stack a -> Stack a
push x r = x :> r
{-# INLINE push #-}

dropN :: Int -> Stack a -> Stack a
dropN 0 s = s
dropN _ EmptyStack = error "dropping too much"
dropN n (_ :> r) = dropN (n-1) r
{-# INLINE dropN #-}

peekAt :: Int -> Stack a -> a
peekAt _ EmptyStack = error "peeking too far"
peekAt 0 (x :> _) = x
peekAt n (_ :> r) = peekAt (n-1) r
{-# INLINE peekAt #-}

swapAt :: (Show a) => Int -> Stack a -> Stack a
swapAt n       EmptyStack         = error $ "swapping " ++ show n ++ " in empty stack"
swapAt n       (_ :> EmptyStack)  = error $ "swapping " ++ show n ++ " in 1 element stack"
swapAt 1       (x :> (y :> r))    = y :> (x :> r)
swapAt 2 (a :> (x :> (y :> r)))   = y :> (x :> (a :> r))
swapAt n (a :> r) = (peekAt (n-1) r) :> (swapAt' (n-1) a r)
    where swapAt' :: Int -> a -> Stack a -> Stack a
          swapAt' 0 b (_ :> s)   = b :> s
          swapAt' m b (x :> s)   = x :> (swapAt' (m-1) b s)
          swapAt' _ _ EmptyStack = error "stack too small for swap"
          {-# INLINE swapAt' #-}
{-# INLINE swapAt #-}

test :: (Eq a, Num a) => Stack a -> Bool
test EmptyStack = error "can not test an empty stack"
test (x :> _) = x /= 0
{-# INLINE test #-}

newtype Blocks = Blocks { unpackBlocks :: Map.Map Int (Instructions -> Stack Int -> Blocks -> IO ()) }

run :: (Instructions, Map.Map Int Instructions) -> IO ()
run (instructions, blocks) = eval instructions mempty lib
  where
    lib = Blocks $ Map.map mapping blocks
    mapping body remaining = eval (body ++ remaining)

-- execute instructions on a stack
eval :: Instructions -> Stack Int -> Blocks -> IO ()
eval (  (ExecuteBlock(body, Always)):rest) s bs = eval (body ++ rest) s bs
eval (  (ExecuteBlock(body, If))    :rest) s bs = if test s then eval (body ++ rest) s bs else eval rest s bs
eval (b@(ExecuteBlock(body, While)) :rest) s bs = if test s then eval (body ++ (b:rest)) s bs else eval rest s bs

-- call
eval ((Call name):rest) s bs = body rest s bs
  where
    body = getBlock name (unpackBlocks bs)
    getBlock bname blocks = case Map.lookup bname blocks of
      Nothing -> error $ "Trying to call unknown block " ++ show bname
      Just content -> content
eval (PrintStack:rest) s bs = do print $ "Stack: " ++ show s; eval rest s bs

-- stack operations
eval ((Push n):rest)      s bs             = eval rest (n :> s) bs
eval ((Pop n):rest)       s bs             = eval rest (dropN n s) bs
eval ((Duplicate n):rest) s bs             = eval rest (push (peekAt (n-1) s) s) bs
eval ((Swap n):rest)      s bs             = eval rest (swapAt n s) bs

eval ((Arithmetic   Not):rest) (0 :> s) bs = eval rest (1 :> s) bs
eval ((Arithmetic   Not):rest) (_ :> s) bs = eval rest (0 :> s) bs
eval ((Arithmetic   op ):rest) (x :> (y :> r)) bs = eval rest (getBinaryOperator op y x :> r) bs
eval ((FArithmetic1 op ):rest) (a :> s) bs = eval rest (op a :> s) bs

eval ((Arithmetic   Not):_) EmptyStack        _ = error "can not invert an empty stack"
eval ((FArithmetic1 _  ):_) EmptyStack        _ = error "can not operate on an empty stack"
eval ((Arithmetic   _  ):_) EmptyStack        _ = error "can not operate on an empty stack"
eval ((Arithmetic   _  ):_) (_ :> EmptyStack) _ = error "can not operate on an empty stack"
eval ((DefineBlock  _  ):_) _                 _ = error "define block where it should not be"

eval [] s _ = print $ "Stack: " ++ show s
