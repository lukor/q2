{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}

module AppendMutableInterpreter (run) where

import IL
import Control.Monad.IO.Class
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as M
import qualified Data.IntMap.Strict as Map
import qualified Data.Map.Strict as MMap

newtype Blocks = Blocks { unpackBlocks :: Map.IntMap (Blocks -> M.IOVector Int -> Int -> IO Int) }

run :: (Instructions, MMap.Map Int Instructions) -> IO [Int]
run (instructions, definitions) = do
  vec <- do
    let blocks = Blocks $ Map.fromList $ map (\(k,is) -> (k, eval is)) $ MMap.toList definitions
    stack <- M.new 1024
    p <- eval instructions blocks stack 0
    printMV stack p
    V.freeze $ M.take p $ M.drop 1 stack
  return $ V.toList vec

printMV :: M.Unbox a => Show a => M.IOVector a -> Int -> IO ()
printMV v p = do
    fv <- V.freeze v
    liftIO $ print $ show p ++ " " ++ (show $ take p $ drop 1 (V.toList fv))

test :: M.IOVector Int -> Int -> IO Bool
test v p = do x <- M.read v p; return (x /= 0)

eval :: Instructions -> Blocks -> M.IOVector Int -> Int -> IO Int
eval ((Arithmetic   Not):_) _ _ 0 = error "can not invert an empty stack"
eval ((FArithmetic1 _  ):_) _ _ _ = error "FArithmetic1 not implemented for current backend"
eval ((Arithmetic   _  ):_) _ _ 0 = error "can not operate on an empty stack"
eval ((Arithmetic   _  ):_) _ _ 1 = error "can not operate on an empty stack"
eval ((DefineBlock  _  ):_) _ _ _ = error "define block where it should not be"

eval   ((ExecuteBlock (body, Always)):rest) b v p = eval (body ++ rest) b v p
eval   ((ExecuteBlock (body, If    )):rest) b v p = do t <- test v p
                                                       if t then eval (body ++ rest) b v p else eval rest b v p
eval a@((ExecuteBlock (body, While )):rest) b v p = do t <- test v p
                                                       if t then do eval (body ++ a) b v p else eval rest b v p
eval   ((Call          name         ):rest) b v p = do let content = Map.lookup name (unpackBlocks b)
                                                       p2 <- case content of
                                                         Nothing -> error $ "could not find blck with name " ++ show name
                                                         Just evaluation -> evaluation b v p
                                                       eval rest b v p2
eval   (PrintStack:rest) b v p = do eval rest b v p

eval ((Push       n  ):rest) b v p = do M.write v np n; eval rest b v np
                                        where np = p+1
eval ((Pop        n  ):rest) b v p = eval rest b v (p-n)
eval ((Duplicate  n  ):rest) b v p = do x <- M.read v (p-n+1); M.write v np x; eval rest b v np
                                        where np = p+1
eval ((Swap       n  ):rest) b v p = do M.swap v p (p-n); eval rest b v p
eval ((Arithmetic Not):rest) b v p = do M.modify v (\x -> if x == 0 then 1 else 0) p; eval rest b v p
eval ((Arithmetic op ):rest) b v p = do y <- M.read v p; M.modify v (\x -> getBinaryOperator op x y) (p-1); eval rest b v (p-1)

eval   [] _ _ p = return p
