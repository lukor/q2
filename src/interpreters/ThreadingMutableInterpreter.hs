{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE BangPatterns #-}

module ThreadingMutableInterpreter (run) where

import qualified IL as IL
import Control.Monad.Cont
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as M
import qualified Data.IntMap.Strict as Map
import qualified Data.Map.Strict as MMap



--------------------------------
-- Instructions
--------------------------------

type Stack = M.IOVector Int
type Instruction = Stack -> Int -> IO (Stack, Int)
type Blocks = Map.IntMap (Instruction -> Instruction)



--------------------------------
-- helper functions
--------------------------------

test :: M.IOVector Int -> Int -> IO Bool
test _ 0 = error "can not read from empty stack"
test v p = do x <- M.read v p; return (x /= 0)
{-# INLINE test #-}

printMV :: M.Unbox a => Show a => M.IOVector a -> Int -> IO ()
printMV v p = do
    fv <- V.freeze v
    liftIO $ print $ show p ++ " " ++ (show $ take p $ drop 1 (V.toList fv))
{-# INLINE printMV #-}

lo :: Int -> Map.IntMap a -> a
lo n bs = check $ Map.lookup n bs
                where
                    check Nothing = error $ "unknown block with name " ++ show n
                    check (Just a) = a
{-# INLINE lo #-}


--------------------------------
-- actions
--------------------------------

-- TODO: check for overflow
push :: Int -> Instruction -> Instruction
push n r v p = do M.write v np n
                  r v np
                    where np = p+1

pop :: Int -> Instruction -> Instruction
pop n r v p = r v $ p-n

-- TODO: check for overflow
duplicate :: Int -> Instruction -> Instruction
duplicate n r v p = do x <- M.read v (p-n+1)
                       M.write v np x
                       r v np
                         where np = p+1

swap :: Int -> Instruction -> Instruction
swap n r v p = do M.swap v p (p-n)
                  r v p

invert :: Instruction -> Instruction
invert r v p = do M.modify v (\x -> if x == 0 then 1 else 0) p
                  r v p

arithmetic :: (Int -> Int -> Int) -> Instruction -> Instruction
arithmetic op r v p = do y <- M.read v p
                         M.modify v (\x -> x `op` y) (p-1)
                         r v $ p-1

printStack :: Instruction -> Instruction
printStack r v p = do printMV v p
                      r v p

call :: Blocks -> Int -> Instruction -> Instruction
call bs name r v p = lo name bs r v p

executeAlways :: (Instruction -> Instruction) -> Instruction -> Instruction
executeAlways b r v p = b r v p

executeIf :: (Instruction -> Instruction) -> Instruction -> Instruction
executeIf b r v p = do t <- test v p
                       if t then b r v p else r v p

executeWhile :: (Instruction -> Instruction) -> Instruction -> Instruction
executeWhile b r v p = do t <- test v p
                          if t then b (\nv np -> executeWhile b r nv np) v p else r v p



--------------------------------
-- translation
--------------------------------

translate :: Blocks -> IL.Instruction -> Instruction -> Instruction
translate _  (IL.Push      n) = push      n
translate _  (IL.Pop       n) = pop       n
translate _  (IL.Duplicate n) = duplicate n
translate _  (IL.Swap      n) = swap      n

translate _  (IL.Arithmetic   IL.Not) = invert
translate _  (IL.Arithmetic   i     ) = arithmetic   (IL.getBinaryOperator i)
-- translate _  (IL.FArithmetic1 i     ) = fArithmetic1 i

translate bs (IL.ExecuteBlock (is, IL.Always)) = executeAlways $ combine $ map (translate bs) is
translate bs (IL.ExecuteBlock (is, IL.If    )) = executeIf     $ combine $ map (translate bs) is
translate bs (IL.ExecuteBlock (is, IL.While )) = executeWhile  $ combine $ map (translate bs) is

translate bs (IL.Call name) = call bs name

translate _  IL.PrintStack = printStack

translate _ x = error $ "can not translate " ++ show x


--------------------------------
-- run
--------------------------------

run :: (IL.Instructions, MMap.Map Int IL.Instructions) -> IO [Int]
run (instructions, definitions) = do
  vec <- do
    let blocks = Map.fromList $ map (\(k,is) -> (k, combine $ map (translate blocks) is)) $ MMap.toList definitions
    stack <- M.new 1024
    (v, p) <- combine (map (translate blocks) instructions) (\v p -> return (v, p)) stack 0
    printMV v p
    V.freeze $ M.take p $ M.drop 1 v
  return $ V.toList vec

combine :: [Instruction -> Instruction] -> Instruction -> Instruction
combine [] r = r
combine (x:xs) r = x $ combine xs r
{-# INLINE combine #-}
