{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE BangPatterns #-}

module ComposingMutableInterpreter (run) where

import qualified IL as IL
import Control.Monad
import Control.Monad.IO.Class
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as M
import qualified Data.IntMap.Strict as Map
import qualified Data.Map.Strict as MMap



--------------------------------
-- Instructions
--------------------------------

type Instruction = (M.IOVector Int, Int) -> IO (M.IOVector Int, Int)
type Blocks = Map.IntMap Instruction



--------------------------------
-- helper functions
--------------------------------

test :: M.IOVector Int -> Int -> IO Bool
test v p = do x <- M.read v p; return (x /= 0)

printMV :: M.Unbox a => Show a => M.IOVector a -> Int -> IO ()
printMV v p = do
    fv <- V.freeze v
    liftIO $ print $ show p ++ " " ++ (show $ take p $ drop 1 (V.toList fv))

lo :: Int -> Map.IntMap a -> a
lo n bs = check $ Map.lookup n bs
                where
                    check Nothing = error $ "unknown block with name " ++ show n
                    check (Just a) = a


--------------------------------
-- actions
--------------------------------

-- TODO: check for overflow
push :: Int -> Instruction
push n (v, p) = do M.write v np n; return (v, np)
                    where np = p+1

pop :: Int -> Instruction
pop n (v, p) = return (v, p-n)

-- TODO: check for overflow
duplicate :: Int -> Instruction
duplicate n (v, p) = do x <- M.read v (p-n+1); M.write v np x; return (v, np)
                        where np = p+1

swap :: Int -> Instruction
swap n (v, p) = do M.swap v p (p-n); return (v, p)

invert :: Instruction
invert (v, p) = do M.modify v (\x -> if x == 0 then 1 else 0) p; return (v, p)

arithmetic :: (Int -> Int -> Int) -> Instruction
arithmetic op (v, p) = do y <- M.read v p; M.modify v (\x -> x `op` y) (p-1); return (v, p-1)

executeAlways :: Instruction -> Instruction
executeAlways b ps = b ps

executeIf :: Instruction -> Instruction
executeIf b (v, p) = do t <- test v p; if t then b (v, p) else return (v, p)

executeWhile :: Instruction -> Instruction
executeWhile b (v, p) = do t <- test v p; if t then do b (v, p) >>= executeWhile b else return (v, p)

call :: Blocks -> Int -> Instruction
call bs name (v, p) = lo name bs (v, p)

printStack :: Instruction
printStack (v, p) = do printMV v p; return (v, p)



--------------------------------
-- translation
--------------------------------

translate :: Blocks -> IL.Instruction -> Instruction
translate _  (IL.Push      n) = push      n
translate _  (IL.Pop       n) = pop       n
translate _  (IL.Duplicate n) = duplicate n
translate _  (IL.Swap      n) = swap      n

translate _  (IL.Arithmetic   IL.Not) = invert
translate _  (IL.Arithmetic   i     ) = arithmetic   (IL.getBinaryOperator i)
-- translate _  (IL.FArithmetic1 i     ) = fArithmetic1 i

translate bs (IL.ExecuteBlock (is, IL.Always)) = executeAlways $ concatM $ map (translate bs) is
translate bs (IL.ExecuteBlock (is, IL.If    )) = executeIf     $ concatM $ map (translate bs) is
translate bs (IL.ExecuteBlock (is, IL.While )) = executeWhile  $ concatM $ map (translate bs) is

translate bs (IL.Call name) = call bs name

translate _  IL.PrintStack = printStack

translate _ x = error $ "can not translate " ++ show x


--------------------------------
-- run
--------------------------------

run :: (IL.Instructions, MMap.Map Int IL.Instructions) -> IO [Int]
run (instructions, definitions) = do
  vec <- do
    let blocks = Map.fromList $ map (\(k,is) -> (k, concatM $ map (translate blocks) is)) $ MMap.toList definitions
    stack <- M.new 1024
    (v, p) <- concatM (map (translate blocks) instructions) (stack, 0)
    printMV v p
    V.freeze $ M.take p $ M.drop 1 v
  return $ V.toList vec

concatM :: [a -> IO a] -> (a -> IO a)
concatM fs = foldr (>=>) return fs
