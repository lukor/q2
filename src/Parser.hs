{-# LANGUAGE ScopedTypeVariables #-}

module Parser (parse) where
import IL
import Data.Maybe
import qualified Data.Map.Strict as Map

data CommandType = CTPush             --  1
                 | CTPop              --  2
                 | CTDuplicate        --  3
                 | CTSwap             --  4
                 | CTArithmetic       --  5
                 | CTCall             --  6
                 | CTStartBlock       --  7
                 | CTExecuteBlock     --  8
                 | CTDefineBlock      --  9
                 | CTPrintStack       -- 10
                 deriving (Show, Enum, Bounded, Eq)

data ArithmeticType = ATAddition       -- 1
                    | ATSubtraction    -- 2
                    | ATMultiplication -- 3
                    | ATDivision       -- 4
                    | ATRemainder      -- 5
                    | ATAnd            -- 6
                    | ATOr             -- 7
                    | ATNot            -- 8
                    | ATEquals         -- 9
                    | ATLessThan       -- 10
                    deriving (Show, Enum, Bounded, Eq)

data ExecType = ETAlways
              | ETIf
              | ETWhile
              deriving (Show, Enum, Bounded, Eq)

safeToEnum :: forall a . (Enum a, Bounded a) => Int -> Maybe a
safeToEnum x
  | fromEnum (minBound::a) <= y && y <= fromEnum (maxBound::a) = Just ((toEnum y)::a)
  | otherwise = Nothing
  where y = x-1

data Lexeme = Lexeme Int Int Int deriving Show

enumerate :: Int -> [a] -> [(Int, a)]
enumerate n = zip [n..]

safeTail :: [a] -> [a]
safeTail (_:xs) = xs
safeTail [] = []

safeHead :: [a] -> Maybe a
safeHead (x:_) = Just x
safeHead [] = Nothing

parseUp :: [(Lexeme, Lexeme)] -> (Instructions, [(Lexeme, Lexeme)])
parseUp chain@(cmd@(Lexeme ct _ _, Lexeme p _ _):xs) = case safeToEnum ct of
  Just CTPush         -> (Push       p  : result, remaining)
  Just CTPop          -> (Pop        p  : result, remaining)
  Just CTDuplicate    -> (Duplicate  p  : result, remaining)
  Just CTSwap         -> (Swap       p  : result, remaining)
  Just CTCall         -> (Call       p  : result, remaining)
  Just CTPrintStack   -> (PrintStack    : result, remaining)
  Just CTArithmetic   -> (Arithmetic at : result, remaining)
    where
      at = case safeToEnum p of
        Just ATAddition       -> Addition
        Just ATSubtraction    -> Subtraction
        Just ATMultiplication -> Multiplication
        Just ATDivision       -> Division
        Just ATRemainder      -> Remainder
        Just ATAnd            -> And
        Just ATOr             -> Or
        Just ATNot            -> Not
        Just ATEquals         -> Equals
        Just ATLessThan       -> LessThan
        _ -> error $ "unknown arithmetic expression type " ++ show p ++ " at " ++ show cmd

  Just CTDefineBlock  -> ([], chain)
  Just CTExecuteBlock -> ([], chain)

  Just CTStartBlock   -> (block : nextResult, nextRemaining)
    where
      block = case safeHead remaining of
        Just blockEndCmd@(Lexeme blockEndType _ _, Lexeme exTypeOrName _ _)
          | c == CTDefineBlock  -> DefineBlock(result, exTypeOrName)
          | c == CTExecuteBlock -> ExecuteBlock(result, et)
          | otherwise -> error "This should not happen"
            where
              et = case safeToEnum exTypeOrName of
                Just ETAlways -> Always
                Just ETIf     -> If
                Just ETWhile  -> While
                _ -> error $ "unknown execution type " ++ show exTypeOrName ++ " at " ++ show blockEndCmd
              c = fromJust $ safeToEnum blockEndType
        Nothing -> error $ "started a block at " ++ show cmd ++ " but no block end was found"
      (nextResult, nextRemaining) = parseUp (safeTail remaining)

  Nothing -> error $ "unknown command type at element: " ++ show cmd
  where
    (result, remaining) = parseUp xs
parseUp [] = ([], [])

parse :: String -> (Instructions, Map.Map Int Instructions)
parse s = extractBlockDefinitions $ oooo $ pairUp $ Parser.lex s
  where
    oooo :: [(Lexeme, Lexeme)] -> Instructions
    oooo (x:xs) = case x of
      (Lexeme ct _ _, _)
        | c `elem` [CTDefineBlock, CTExecuteBlock] -> error $ "dangling end block statement at " ++ show x
        | otherwise -> result ++ oooo remaining
        where
          (result, remaining) = parseUp (x:xs)
          c = fromJust $ safeToEnum ct
    oooo [] = []


lex :: String -> [Lexeme]
lex s = concat $ map lexLine $ enumerate 1 $ map (enumerate 1) $ lines s

lexLine :: (Int, [(Int, Char)]) -> [Lexeme]
lexLine (line, (pos,'q'):xs) = Lexeme (length content + 1) line pos : lexLine (line, remaining)
  where
    (content, remaining) = break notQ xs
    notQ (_, c) = 'q' /= c
lexLine (line, _:xs) = lexLine (line, xs)
lexLine (_,    [])           = []


pairUp :: (Show a) => [a] -> [(a,a)]
pairUp list = case list of
  x:y:xs -> (x,y) : pairUp xs
  [x]    -> error $ "uneven number of elements at element: " ++ show x
  []     -> []

extractBlockDefinitions :: Instructions -> (Instructions, Map.Map Int Instructions)
extractBlockDefinitions insts = eb insts [] Map.empty
  where
    eb :: Instructions -> Instructions -> Map.Map Int Instructions -> (Instructions, Map.Map Int Instructions)
    eb [] ins blocks = (reverse ins, blocks)
    eb (x:xs) ins blocks = case x of
        DefineBlock(binsts, bname) -> case Map.lookup bname blocks of
          Just _  -> error $ "function " ++ show bname ++ " has been redefined"
          Nothing -> eb xs ins $ Map.insert bname binsts blocks
        inst -> eb xs (inst : ins) blocks
