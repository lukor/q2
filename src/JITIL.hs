{-# LANGUAGE DeriveGeneric #-}

module JITIL where

import qualified IL
import GHC.Generics (Generic)
import Control.DeepSeq
import qualified Data.Map.Strict as Map
type BlockName = Integer

data ExecutionType = Always
                   | If
                   | While
                     deriving (Show, Eq, Generic)
instance NFData ExecutionType

data ArithmeticExpression = Addition
                          | Subtraction
                          | Multiplication
                          | Division
                          | Remainder
                          | And
                          | Or
                          | Not
                          | Equals
                          | LessThan
                            deriving (Show, Eq, Generic)
instance NFData ArithmeticExpression

data Instruction = Push(Integer)
                 | Pop(Integer)
                 | Duplicate(Integer)
                 | Swap(Integer)
                 | Arithmetic(ArithmeticExpression)
                 | ExecuteBlock(Instructions, ExecutionType)
                 | DefineBlock(Instructions, BlockName)
                 | Call(BlockName)
                 | PrintStack
                 -- internal instructions
                 | FArithmetic1(Integer -> Integer)
                 deriving Generic
instance NFData Instruction

type Instructions = [Instruction]

instance Show Instruction where
  show (Push         x         ) = "(Push " ++ show x ++ ")"
  show (Pop          x         ) = "(Pop " ++ show x ++ ")"
  show (Duplicate    x         ) = "(Duplicate " ++ show x ++ ")"
  show (Swap         x         ) = "(Swap " ++ show x ++ ")"
  show (Arithmetic   x         ) = "(Arithmetic " ++ show x ++ ")"
  show (ExecuteBlock(ins, et)  ) = "(ExecuteBlock (" ++ show ins ++ ", " ++ show et ++ "))"
  show (DefineBlock (ins, name)) = "(DefineBlock (" ++ show ins ++ ", " ++ show name ++ "))"
  show (Call         name      ) = "(Call " ++ show name ++ ")"
  show (PrintStack             ) = "(PrintStack)"
  show (FArithmetic1 _         ) = "(FArithmetic1)"


toJITIL :: IL.Instruction -> Instruction
toJITIL (IL.Push      n) = Push      $ toInteger n
toJITIL (IL.Pop       n) = Pop       $ toInteger n
toJITIL (IL.Duplicate n) = Duplicate $ toInteger n
toJITIL (IL.Swap      n) = Swap      $ toInteger n

toJITIL (IL.Arithmetic IL.Addition      ) = Arithmetic Addition
toJITIL (IL.Arithmetic IL.Subtraction   ) = Arithmetic Subtraction
toJITIL (IL.Arithmetic IL.Multiplication) = Arithmetic Multiplication
toJITIL (IL.Arithmetic IL.Division      ) = Arithmetic Division
toJITIL (IL.Arithmetic IL.Remainder     ) = Arithmetic Remainder
toJITIL (IL.Arithmetic IL.And           ) = Arithmetic And
toJITIL (IL.Arithmetic IL.Or            ) = Arithmetic Or
toJITIL (IL.Arithmetic IL.Not           ) = Arithmetic Not
toJITIL (IL.Arithmetic IL.Equals        ) = Arithmetic Equals
toJITIL (IL.Arithmetic IL.LessThan      ) = Arithmetic LessThan

toJITIL (IL.ExecuteBlock (is, IL.Always)) = ExecuteBlock (map toJITIL is, Always)
toJITIL (IL.ExecuteBlock (is, IL.If    )) = ExecuteBlock (map toJITIL is, If)
toJITIL (IL.ExecuteBlock (is, IL.While )) = ExecuteBlock (map toJITIL is, While)

toJITIL (IL.Call name) = Call $ toInteger name

toJITIL IL.PrintStack = PrintStack

toJITIL (IL.DefineBlock (is, name)) = DefineBlock (map toJITIL is, toInteger name)

toJITIL x = error $ "can not translate " ++ show x ++ " for JIT IL"

translate :: (IL.Instructions, Map.Map Int IL.Instructions) -> (Instructions, Map.Map Integer Instructions)
translate (is, bs) = (map toJITIL is, Map.fromList $ map (\(k, v) -> (toInteger k, map toJITIL v)) $ Map.toList bs)
