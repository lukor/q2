module Helpers where

import Data.Maybe
import qualified Data.Map.Strict as Map

cartesian :: [[a]] -> [[a]]
cartesian [] = [[]]
cartesian (xs:xss) = [ x:ys | x<- xs, ys <-yss ]
    where yss = cartesian xss

mapEntry :: Ord k => k -> Map.Map k a -> a
mapEntry k m = fromJust $ Map.lookup k m
