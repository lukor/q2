module Frontend (start, BackendConfig (..), ProgramConfig (..)) where
import qualified DummyBackend as Dummy
import qualified ComposeADTInterpreter as Interpreter
import qualified JITBackend as JIT
import qualified Data.Map.Strict as Map
import Parser
import IL
import qualified JITIL

data BackendConfig =
    InterpreterBackend |
    JITBackend |
    DummyBackend
    deriving Show

data ProgramConfig = ProgramConfig {
      file :: String,
      useBackend :: BackendConfig
    }
    deriving Show

run :: BackendConfig -> (Instructions, Map.Map Int Instructions) -> IO ()
run DummyBackend = Dummy.run
run InterpreterBackend = (\i -> do _ <- Interpreter.run i; return ())
run JITBackend = (JIT.run . JITIL.translate)

start :: ProgramConfig -> IO ()
start config = do
  program <- parseFile $ file config
  run (useBackend config) program

parseFile :: String -> IO (Instructions, Map.Map Int Instructions)
parseFile filename = do
  program <- readFile filename
  return $ toIL program

toIL :: String -> (Instructions, Map.Map Int Instructions)
toIL = parse
